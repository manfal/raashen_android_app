package com.utilstore.analytics.logging;

/**
 * Created by anfal on 12/12/2015.
 */
public interface ILog {
    void log(int logLevel, String tag, String message, Throwable exception);
}
