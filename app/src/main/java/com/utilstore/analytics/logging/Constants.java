package com.utilstore.analytics.logging;

import android.util.Log;

/**
 * Created by anfal on 12/12/2015.
 */
public class Constants {
    //Levels
    public static final int VERBOSE = Log.VERBOSE;
    public static final int DEBUG = Log.DEBUG;
    public static final int INFO = Log.INFO;
    public static final int WARN = Log.WARN;
    public static final int ERROR = Log.ERROR;
    public static final int ASSERT = Log.ASSERT;
    //Tags
    public static final String TAG_ERROR = "Error";

    private Constants() {
    }
}
