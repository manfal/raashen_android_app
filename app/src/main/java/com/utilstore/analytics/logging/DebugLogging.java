package com.utilstore.analytics.logging;

import android.util.Log;

/**
 * Created by anfal on 12/12/2015.
 */
public class DebugLogging implements ILog {
    @Override
    public void log(int logLevel, String tag, String message, Throwable exception) {
        Log.d(tag, logLevel + ": " + message, exception);
    }
}
