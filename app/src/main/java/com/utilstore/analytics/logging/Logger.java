package com.utilstore.analytics.logging;

import com.utilstore.app.BuildConfig;

/**
 * Created by anfal on 12/12/2015.
 */
public class Logger {
    private Logger() {
    }

    public static ILog getLogger() {
        if (BuildConfig.DEBUG) {
            return new DebugLogging();
        } else {
            return new ReleaseLogging();
        }
    }
}
