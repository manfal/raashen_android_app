package com.utilstore.utilities;

import android.util.Log;

import com.utilstore.analytics.logging.Constants;
import com.utilstore.analytics.logging.ILog;
import com.utilstore.analytics.logging.Logger;
import com.utilstore.components.dataobjects.CartItemDataObject;

import java.util.List;

/**
 * Created by anfal on 1/12/2016.
 */
public class ItemCheckoutUtility {

    private static ItemCheckoutUtility itemCheckoutUtility = null;
    private ILog logger = null;

    private ItemCheckoutUtility() {
        this.logger = Logger.getLogger();
    }

    public static ItemCheckoutUtility getInstance() {

        if (null == itemCheckoutUtility) {
            itemCheckoutUtility = new ItemCheckoutUtility();
        }

        return itemCheckoutUtility;
    }

    public int getDiscountedPrice(int price, int discount) {
        float discountedPrice = 0;
        try {
            discountedPrice = (float) price - Math.round(((float) price * ((float) discount / (float) 100)));
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
        return Math.round(discountedPrice);
    }

    public int getCheckoutPriceWithoutDeliveryCharges(List<CartItemDataObject> cartItemDataObjects) {
        int subTotal = 0;
        try {
            for (CartItemDataObject cartItemDataObject : cartItemDataObjects) {
                subTotal += this.getItemPriceBasedOnQuantity(cartItemDataObject.getItemPrice(), cartItemDataObject.getQuantityToCheckout(), cartItemDataObject.getItemDiscount());
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
        return subTotal;
    }

    public int getItemPriceBasedOnQuantity(int price, int quantity, int discount) {
        return this.getDiscountedPrice(price, discount) * quantity;
    }
}