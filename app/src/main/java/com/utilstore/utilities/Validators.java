package com.utilstore.utilities;

import android.util.Patterns;

/**
 * Created by anfal on 12/9/2015.
 */
public class Validators {
    private static Validators validatorInstance = null;

    private Validators() {
    }

    public static Validators getInstance() {
        if (null == validatorInstance) {
            validatorInstance = new Validators();
        }
        return validatorInstance;
    }

    public boolean isEmailValid(String email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}
