package com.utilstore.actions;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.utilstore.analytics.logging.Constants;
import com.utilstore.analytics.logging.ILog;
import com.utilstore.analytics.logging.Logger;
import com.utilstore.app.R;
import com.utilstore.components.adapter.MyAccountFragmentAdapter;
import com.utilstore.components.fragments.MyAccountAddressesFragment;
import com.utilstore.components.fragments.MyAccountPersonalInformationFragment;
import com.utilstore.components.interfaces.IInternetConnectionObject;
import com.utilstore.components.observers.CartObserver;
import com.utilstore.components.observers.LeftNavigationObserver;
import com.utilstore.utilities.InternetConnectionUtility;
import com.utilstore.utilities.Utilities;

/**
 * Created by anfal on 1/1/2016.
 */
public class MyAccountActions implements IInternetConnectionObject {

    private static MyAccountActions myAccountActionsInstance = null;
    private AppCompatActivity appCompatActivity = null;
    private ILog logger = null;
    private DrawerLayout leftNavDrawer = null;
    private Toolbar toolbar = null;
    private RecyclerView leftMenuView = null;
    private ViewPager viewPager = null;
    private TabLayout tabLayout = null;
    private InternetConnectionUtility internetConnectionUtility = null;

    private MyAccountActions(AppCompatActivity appCompatActivity) {
        this.appCompatActivity = appCompatActivity;
        this.logger = Logger.getLogger();
        this.leftNavDrawer = (DrawerLayout) this.appCompatActivity.findViewById(R.id.myaccount_drawer_layout);
        this.toolbar = (Toolbar) this.appCompatActivity.findViewById(R.id.myaccount_page_toolbar);
        this.appCompatActivity.setSupportActionBar(this.getToolbar());
        LeftNavigationObserver.setLeftNavigationObserverInstance(this.appCompatActivity, this.leftNavDrawer, this.toolbar);
        this.viewPager = (ViewPager) this.appCompatActivity.findViewById(R.id.myaccount_category_pager);
        this.tabLayout = (TabLayout) this.appCompatActivity.findViewById(R.id.myaccount_category_tabs);
        this.internetConnectionUtility = new InternetConnectionUtility(this.appCompatActivity);
        this.internetConnectionUtility.registerAsObserver(this);
        CartObserver.setCartObserverInstance(this.appCompatActivity);
    }

    public static MyAccountActions getMyAccountActionsInstance() {
        return myAccountActionsInstance;
    }

    public static void setMyAccountActionsInstance(AppCompatActivity appCompatActivity) {
        if (null == myAccountActionsInstance) {
            myAccountActionsInstance = new MyAccountActions(appCompatActivity);
        }
    }

    public void destroyMyAccountActionsInstance() {
        if (null != myAccountActionsInstance) {
            myAccountActionsInstance = null;
        }
    }

    public DrawerLayout getLeftNavDrawer() {
        return leftNavDrawer;
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    public RecyclerView getLeftMenuView() {
        return leftMenuView;
    }

    public void setupMyAccountFragments() {
        try {
            if (this.internetConnectionUtility.checkInternetConnectivity()) {
                this.tabLayout.setTabMode(TabLayout.MODE_FIXED);
                Utilities.getInstance().configureTabLook(this.tabLayout);
                MyAccountFragmentAdapter myAccountFragmentAdapter = new MyAccountFragmentAdapter(this.appCompatActivity.getSupportFragmentManager());
                myAccountFragmentAdapter.addFragment(new MyAccountPersonalInformationFragment(), this.appCompatActivity.getString(R.string.fragment_account_detail_title));
                myAccountFragmentAdapter.addFragment(new MyAccountAddressesFragment(), this.appCompatActivity.getString(R.string.fragment_account_detail_addresses));
                this.viewPager.setAdapter(myAccountFragmentAdapter);
                this.tabLayout.setupWithViewPager(this.viewPager);
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void startReconnecting() {

    }

    @Override
    public void tryToReconnect() {
        try {
            this.setupMyAccountFragments();
            LeftNavigationObserver.getLeftNavigationObserverInstance().fetfchLeftNavigation();
            CartObserver.getCartObserverInstance().instantiateCart();
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }
}
