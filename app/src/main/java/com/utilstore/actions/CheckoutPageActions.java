package com.utilstore.actions;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.utilstore.analytics.LogEvents;
import com.utilstore.analytics.logging.Constants;
import com.utilstore.analytics.logging.ILog;
import com.utilstore.analytics.logging.Logger;
import com.utilstore.app.R;
import com.utilstore.app.StorePage;
import com.utilstore.components.CustomComponents;
import com.utilstore.components.dataobjects.CartItemDataObject;
import com.utilstore.components.dataobjects.DeliveryChargesInfoDataObject;
import com.utilstore.components.interfaces.IAddressResponseSenderReceiver;
import com.utilstore.components.interfaces.IInternetConnectionObject;
import com.utilstore.components.interfaces.IPhoneNumberVerificationInterface;
import com.utilstore.components.listeners.CheckoutPageButtonListener;
import com.utilstore.components.listeners.CheckoutPhoneNumberListener;
import com.utilstore.components.listeners.MyAccountVerifyPhoneNumberListener;
import com.utilstore.components.listeners.NewAddressListener;
import com.utilstore.components.listeners.ResetPhoneNumberListener;
import com.utilstore.components.observers.LeftNavigationObserver;
import com.utilstore.components.tasks.PostCheckoutCleanUpTask;
import com.utilstore.database.ClientDB;
import com.utilstore.datastore.CheckoutPageController;
import com.utilstore.utilities.InternetConnectionUtility;
import com.utilstore.utilities.ItemCheckoutUtility;
import com.utilstore.utilities.Utilities;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.utilstore.analytics.logging.Constants.TAG_ERROR;
import static com.utilstore.constants.Constants.ADDRESSE_LABELS;
import static com.utilstore.constants.Constants.ADDRESS_LABEL;
import static com.utilstore.constants.Constants.ADDRESS_PARAM;
import static com.utilstore.constants.Constants.DELIVERY_ADDRESS;
import static com.utilstore.constants.Constants.EMAIL;
import static com.utilstore.constants.Constants.EMAIL_PARAM;
import static com.utilstore.constants.Constants.ERROR;
import static com.utilstore.constants.Constants.IS_MOBILE_NUMBER_CONFIRMED;
import static com.utilstore.constants.Constants.ITEMS_TO_CHECKOUT;
import static com.utilstore.constants.Constants.ITEM_DISCOUNT;
import static com.utilstore.constants.Constants.ITEM_ID;
import static com.utilstore.constants.Constants.ITEM_NAME;
import static com.utilstore.constants.Constants.ITEM_PRICE;
import static com.utilstore.constants.Constants.ITEM_QUANTITY_TO_CHECKOUT;
import static com.utilstore.constants.Constants.ITEM_WEIGHT_OR_QUANTITY;
import static com.utilstore.constants.Constants.MESSAGE;
import static com.utilstore.constants.Constants.MOBILE_NUMBER;
import static com.utilstore.constants.Constants.MOBILE_NUMBER_PARAM;
import static com.utilstore.constants.Constants.NEW_ADDRESS;
import static com.utilstore.constants.Constants.OH_DELIVERY_CHARGES;
import static com.utilstore.constants.Constants.OH_ITEM_CHARGED_PRICE;
import static com.utilstore.constants.Constants.OH_ITEM_QUANTITY;
import static com.utilstore.constants.Constants.STATUS;
import static com.utilstore.constants.Constants.SUCCESS;

/**
 * Created by anfal on 1/27/2016.
 */
public class CheckoutPageActions implements IAddressResponseSenderReceiver, IPhoneNumberVerificationInterface, IInternetConnectionObject {

    private static CheckoutPageActions checkoutPageActionsInstance = null;
    private final int FORWARD = 1;
    private final int BACKWARD = 0;
    private final int THANKS_FOR_SHOPPING = -1;
    private ILog logger = null;
    private AppCompatActivity appCompatActivity = null;
    private ScrollView preCheckoutInfo, preCheckoutInvoiceReview = null;
    private Button checkoutForwardButton, checkoutBackwardButton = null;
    private CheckoutPageController checkoutPageController = null;
    private RelativeLayout checkoutContainerBody = null;
    private CardView mobileNumberCard = null;
    private ClientDB clientDB = null;
    private Spinner addressSpinner = null;
    private ProgressBar progressBar = null;
    private ArrayAdapter<String> addressSpinnerAdapter = null;
    private TextView deliveryAddressText, orderCost, deliveryCharges, grandTotal, resetPhoneNumber, phoneNumber = null;
    private LinearLayout itemDetailLayout = null;
    private int deliverCharges = 0;
    private String addressForCheckout = null;
    private RelativeLayout thankYouForShopping = null;
    private InternetConnectionUtility internetConnectionUtility = null;

    private CheckoutPageActions(AppCompatActivity appCompatActivity) {
        this.logger = Logger.getLogger();

        try {
            this.appCompatActivity = appCompatActivity;
            Toolbar toolbar = (Toolbar) this.appCompatActivity.findViewById(R.id.checkout_page_toolbar);
            this.appCompatActivity.setSupportActionBar(toolbar);
            this.clientDB = new ClientDB(this.appCompatActivity);
            DrawerLayout drawerLayout = (DrawerLayout) this.appCompatActivity.findViewById(R.id.checkout_page_drawer_layout);
            LeftNavigationObserver.setLeftNavigationObserverInstance(this.appCompatActivity, drawerLayout, toolbar);
            this.preCheckoutInfo = (ScrollView) this.appCompatActivity.findViewById(R.id.pre_checkout_info_layout);
            this.preCheckoutInvoiceReview = (ScrollView) this.appCompatActivity.findViewById(R.id.pre_checkout_review_invoice_layout);
            this.checkoutBackwardButton = (Button) this.appCompatActivity.findViewById(R.id.checkout_backward_button);
            this.checkoutForwardButton = (Button) this.appCompatActivity.findViewById(R.id.checkout_forward_button);
            this.progressBar = (ProgressBar) this.appCompatActivity.findViewById(R.id.checkout_page_progress);
            this.checkoutBackwardButton.setOnClickListener(new CheckoutPageButtonListener());
            this.checkoutForwardButton.setOnClickListener(new CheckoutPageButtonListener());
            this.checkoutPageController = new CheckoutPageController(this.appCompatActivity);
            this.internetConnectionUtility = new InternetConnectionUtility(this.appCompatActivity);
            this.internetConnectionUtility.registerAsObserver(this);
            this.fetchPreCheckoutInfo();
            this.addressSpinner = (Spinner) this.appCompatActivity.findViewById(R.id.pre_defined_address_label_spinner);
            this.phoneNumber = (TextView) this.appCompatActivity.findViewById(R.id.enter_phone_number_link);
            this.mobileNumberCard = (CardView) this.appCompatActivity.findViewById(R.id.mobile_number_prompt_card);
            this.checkoutContainerBody = (RelativeLayout) this.appCompatActivity.findViewById(R.id.checkout_container_body);
            CardView addNewAddressCard = (CardView) this.appCompatActivity.findViewById(R.id.add_new_address_card);
            addNewAddressCard.setOnClickListener(new NewAddressListener(this));
            this.deliveryAddressText = (TextView) this.appCompatActivity.findViewById(R.id.delivery_address_text);
            this.itemDetailLayout = (LinearLayout) this.appCompatActivity.findViewById(R.id.checkout_item_review_info);
            this.orderCost = (TextView) this.appCompatActivity.findViewById(R.id.oh_det_order_cost);
            this.deliveryCharges = (TextView) this.appCompatActivity.findViewById(R.id.oh_det_delivery_charges);
            this.grandTotal = (TextView) this.appCompatActivity.findViewById(R.id.oh_det_grand_total);
            this.thankYouForShopping = (RelativeLayout) this.appCompatActivity.findViewById(R.id.thanks_bye_bye);
            this.resetPhoneNumber = (TextView) this.appCompatActivity.findViewById(R.id.reset_phone_number_link);
            this.resetPhoneNumber.setOnClickListener(new ResetPhoneNumberListener());
            Utilities.getInstance().decorateHyperLink(this.resetPhoneNumber);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public static CheckoutPageActions getCheckoutPageActionsInstance() {
        return checkoutPageActionsInstance;
    }

    public static void setCheckoutPageActionsInstance(AppCompatActivity appCompatActivity) {
        if (null == checkoutPageActionsInstance) {
            checkoutPageActionsInstance = new CheckoutPageActions(appCompatActivity);
        }
    }

    public void destroyCheckoutPageActionsInstance() {
        if (null != checkoutPageActionsInstance) {
            checkoutPageActionsInstance = null;
        }
    }

    public void checkoutButtonListenEvent(String textOnButton) {
        try {
            if (textOnButton.equals(this.appCompatActivity.getString(R.string.next_text))) {
                if (this.checkIfUserIsOkToProceed()) {
                    this.progressBar.setVisibility(View.VISIBLE);
                    this.initiateInvoiceInfoFetch();
                } else {
                    Toast.makeText(this.appCompatActivity, this.appCompatActivity.getString(R.string.alert_incomplete_information), Toast.LENGTH_LONG).show();
                }
            } else if (textOnButton.equals(this.appCompatActivity.getString(R.string.cancel_text))) {
                Utilities.getInstance().startNewActivityByFinishingOlder(this.appCompatActivity, StorePage.class);
            } else if (textOnButton.equals(this.appCompatActivity.getString(R.string.checkout_button_text))) {
                this.initiateCheckout();
            } else if (textOnButton.equals(this.appCompatActivity.getString(R.string.back_text))) {
                this.fadeInLayout(BACKWARD);
                this.checkoutBackwardButton.setText(this.appCompatActivity.getString(R.string.cancel_text));
                this.checkoutForwardButton.setText(this.appCompatActivity.getString(R.string.next_text));
                this.itemDetailLayout.removeAllViews();
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    private void fadeInLayout(int direction) {
        try {
            Animation fadeInAnimation = AnimationUtils.loadAnimation(this.appCompatActivity, android.R.anim.fade_in);
            switch (direction) {
                case FORWARD:
                    this.preCheckoutInvoiceReview.setVisibility(View.VISIBLE);
                    this.preCheckoutInvoiceReview.startAnimation(fadeInAnimation);
                    this.preCheckoutInfo.setVisibility(View.GONE);
                    break;
                case BACKWARD:
                    this.preCheckoutInfo.setVisibility(View.VISIBLE);
                    this.preCheckoutInfo.setAnimation(fadeInAnimation);
                    this.preCheckoutInvoiceReview.setVisibility(View.GONE);
                    break;
                case THANKS_FOR_SHOPPING:
                    this.thankYouForShopping.setVisibility(View.VISIBLE);
                    this.thankYouForShopping.setAnimation(fadeInAnimation);
                    this.preCheckoutInvoiceReview.setVisibility(View.GONE);
                    this.checkoutForwardButton.setEnabled(false);
                    this.checkoutBackwardButton.setEnabled(false);
                    break;
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    private void populateAddressSpinner(JSONArray addressArray) throws Exception {
        List<String> addressList = new ArrayList<>();
        String defaultAddressString = this.appCompatActivity.getString(R.string.choose_an_existing_address);
        addressList.add(defaultAddressString);
        for (int i = 0; i < addressArray.length(); i++) {
            JSONObject jsonObject = addressArray.getJSONObject(i);
            addressList.add(jsonObject.getString(ADDRESS_LABEL));
        }
        this.addressSpinnerAdapter = new ArrayAdapter<>(this.appCompatActivity, R.layout.simple_spinner_item, addressList);
        this.addressSpinner.setAdapter(this.addressSpinnerAdapter);
    }

    public void fetchPreCheckoutInfo() {
        try {
            if (this.internetConnectionUtility.checkInternetConnectivity()) {
                this.checkoutPageController.getPreCheckoutData(this.clientDB.getUserEmail());
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    private void setUpVerificationCodeString() throws Exception {
        this.phoneNumber.setText(this.appCompatActivity.getString(R.string.verify_number_text));
        this.phoneNumber.setOnClickListener(new MyAccountVerifyPhoneNumberListener(this));
        Utilities.getInstance().decorateHyperLink(this.phoneNumber);
    }

    private void setUpPhoneNumberString() throws Exception {
        this.phoneNumber.setText(this.appCompatActivity.getString(R.string.enter_phone_number_text));
        this.phoneNumber.setOnClickListener(new CheckoutPhoneNumberListener());
        Utilities.getInstance().decorateHyperLink(this.phoneNumber);
    }

    private void setMobileNumberCard(String mobileNumber, int isConfirmed) throws Exception {
        if (mobileNumber.equals("null")) {
            this.setUpPhoneNumberString();
            this.mobileNumberCard.setVisibility(View.VISIBLE);
        } else if (ERROR == isConfirmed) {
            this.setUpVerificationCodeString();
            this.resetPhoneNumber.setVisibility(View.VISIBLE);
            this.mobileNumberCard.setVisibility(View.VISIBLE);
        }
    }

    public void populatePreCheckoutData(JSONObject response) {
        try {
            JSONArray addressArray = response.getJSONArray(ADDRESSE_LABELS);
            this.populateAddressSpinner(addressArray);
            this.setMobileNumberCard(response.getString(MOBILE_NUMBER), response.getInt(IS_MOBILE_NUMBER_CONFIRMED));
            this.checkoutContainerBody.setVisibility(View.VISIBLE);
            this.progressBar.setVisibility(View.GONE);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    private boolean checkIfUserIsOkToProceed() {
        boolean isOkToProceed = false;
        try {
            String defaultAddressString = this.appCompatActivity.getString(R.string.choose_an_existing_address);
            boolean isDefaultAddressString = this.addressSpinner.getSelectedItem().equals(defaultAddressString);
            boolean isMobileNumberVerified = this.mobileNumberCard.getVisibility() == View.GONE;
            if (!isDefaultAddressString && isMobileNumberVerified) {
                isOkToProceed = true;
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
        return isOkToProceed;
    }


    @Override
    public void openNewAddressDialog() {
        try {
            CustomComponents.getInstance().newAddressDialog(this.appCompatActivity, this);
        } catch (Exception ex) {
            this.logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void sendNewAddressRequest(String label, String address, String city) {
        try {
            if (!label.equals("") && !address.equals("") && !city.equals("")) {
                this.progressBar.setVisibility(View.VISIBLE);
                String email = this.clientDB.getUserEmail();
                NewAddressRequestAction newAddressRequestAction = new NewAddressRequestAction();
                newAddressRequestAction.sendNewAddressRequest(this.appCompatActivity, this, label, address, city, email);
            } else {
                Toast.makeText(this.appCompatActivity, this.appCompatActivity.getString(R.string.incomplete_info_cannot_proceed), Toast.LENGTH_LONG).show();
            }
        } catch (Exception ex) {
            this.logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void receiveNewAddressResponse(JSONObject response) {
        try {
            JSONObject jsonObject = response.getJSONObject(NEW_ADDRESS);
            String addressLabel = jsonObject.getString(ADDRESS_LABEL);
            this.addressSpinnerAdapter.add(addressLabel);
            this.addressSpinner.setSelection(this.addressSpinnerAdapter.getPosition(addressLabel), false);
            FlurryAgent.logEvent(
                    LogEvents.EVENT_ID_NEW_ADDRESS_ADDITION,
                    LogEvents.getEventData(
                            LogEvents.EVENT_ID_NEW_ADDRESS_ADDITION,
                            new String[]{
                                    LogEvents.TRIGGERED_ADDRESS_ADDITION_CHECKOUT
                            }
                    )
            );
            this.progressBar.setVisibility(View.GONE);
        } catch (Exception ex) {
            this.logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void showPhoneNumerDialog() {
        try {
            CustomComponents.getInstance().enterPhoneNumberDialog(this.appCompatActivity);
        } catch (Exception ex) {
            this.logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void sendNewPhoneNumberRequest(String phoneNumber) {
        try {
            if (!phoneNumber.equals("")) {
                this.progressBar.setVisibility(View.VISIBLE);
                HashMap<String, String> newAddressInfo = new HashMap<>();
                String email = this.clientDB.getUserEmail();
                newAddressInfo.put(EMAIL_PARAM, email);
                newAddressInfo.put(MOBILE_NUMBER_PARAM, phoneNumber);
                this.checkoutPageController.sendPhoneNumberRequest(newAddressInfo);
            } else {
                Toast.makeText(this.appCompatActivity, this.appCompatActivity.getString(R.string.incomplete_info_cannot_proceed), Toast.LENGTH_LONG).show();
            }
        } catch (Exception ex) {
            this.logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void postPhoneNumberAdditionTasks(JSONObject jsonObject) {
        try {
            //Remove mobile number card visibility and uncomment above two lines to return previous sms check functionality.
            /*this.setUpVerificationCodeString();
            this.resetPhoneNumber.setVisibility(View.VISIBLE);*/
            this.mobileNumberCard.setVisibility(View.GONE);
            this.progressBar.setVisibility(View.GONE);
            //Toast.makeText(this.appCompatActivity, jsonObject.getString(MESSAGE), Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            this.logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void handleVerifyPhoneNumberLinkClick() {
        try {
            CustomComponents.getInstance().verifyPhoneNumberDialog(this.appCompatActivity, this);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    @Override
    public void handlePhoneNumberVerification(String verificationCode) {
        try {
            if (!verificationCode.equals("")) {
                this.progressBar.setVisibility(View.VISIBLE);
                FlurryAgent.logEvent(
                        LogEvents.EVENT_ID_NUMBER_VERIFICATION,
                        LogEvents.getEventData(
                                LogEvents.EVENT_ID_NUMBER_VERIFICATION,
                                new String[]{
                                        LogEvents.TRIGGERED_NUMBER_VERIFICATION_CHECKOUT
                                }
                        )
                );
                String email = this.clientDB.getUserEmail();
                PhoneNumberVerificationAction phoneNumberVerificationAction = new PhoneNumberVerificationAction();
                phoneNumberVerificationAction.sendVerficationCodeConfirmationRequest(this.appCompatActivity, this, verificationCode, email);
            } else {
                Toast.makeText(this.appCompatActivity, this.appCompatActivity.getString(R.string.incomplete_info_cannot_proceed), Toast.LENGTH_LONG).show();
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    @Override
    public void postVerificationChores(JSONObject jsonObject) {
        try {
            this.mobileNumberCard.setVisibility(View.GONE);
            if (SUCCESS == Integer.parseInt(jsonObject.getString(STATUS))) {
                this.progressBar.setVisibility(View.GONE);
            }
            Toast.makeText(this.appCompatActivity, jsonObject.getString(MESSAGE), Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    public void initiateInvoiceInfoFetch() {
        try {
            String email = this.clientDB.getUserEmail();
            String addressLabel = (String) this.addressSpinner.getSelectedItem();
            this.checkoutPageController.fetchInvoiceReviewData(email, addressLabel);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    public void loadInvoiceReviewData(JSONObject response) {
        try {
            this.addressForCheckout = response.getString(DELIVERY_ADDRESS);
            this.deliveryAddressText.setText(this.addressForCheckout);
            this.populateItemDetailLayoutFromDB();
            this.setBillingInfo();
            this.progressBar.setVisibility(View.GONE);
            this.fadeInLayout(FORWARD);
            this.checkoutBackwardButton.setText(this.appCompatActivity.getString(R.string.back_text));
            this.checkoutForwardButton.setText(this.appCompatActivity.getString(R.string.checkout_button_text));
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    private void setBillingInfo() {
        try {
            DeliveryChargesInfoDataObject deliveryChargesInfoDataObject = this.clientDB.getDeliveryChargesInfo();
            int orderCost = ItemCheckoutUtility.getInstance().getCheckoutPriceWithoutDeliveryCharges(this.clientDB.getOrderHistoryItemsFromDB());
            if (deliveryChargesInfoDataObject.getThresholdPrice() > orderCost) {
                this.deliverCharges = deliveryChargesInfoDataObject.getChargedPrice();
            }

            this.orderCost.setText(
                    this.appCompatActivity.getString(
                            R.string.order_cost_text_template,
                            orderCost
                    )
            );

            this.deliveryCharges.setText(
                    this.appCompatActivity.getString(
                            R.string.delivery_charges_text_template,
                            this.deliverCharges
                    )
            );

            this.grandTotal.setText(
                    this.appCompatActivity.getString(
                            R.string.grand_total_text_template,
                            orderCost + this.deliverCharges
                    )
            );
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    private void populateItemDetailLayoutFromDB() {
        try {
            List<CartItemDataObject> cartItemDataObjects = this.clientDB.getOrderHistoryItemsFromDB();

            ArrayList<HashMap<String, String>> itemList = new ArrayList<>();
            for (CartItemDataObject cartItemDataObject : cartItemDataObjects) {
                HashMap<String, String> item = new HashMap<>();
                item.put(
                        OH_ITEM_QUANTITY,
                        this.appCompatActivity.getString(
                                R.string.order_item_quantity_name_template,
                                cartItemDataObject.getQuantityToCheckout(),
                                cartItemDataObject.getItemName()
                        )
                );
                item.put(
                        OH_ITEM_CHARGED_PRICE,
                        this.appCompatActivity.getString(
                                R.string.currency,
                                ItemCheckoutUtility.getInstance().getItemPriceBasedOnQuantity(cartItemDataObject.getItemPrice(), cartItemDataObject.getQuantityToCheckout(), cartItemDataObject.getItemDiscount())
                        )
                );
                itemList.add(item);
            }
            ListAdapter itemInfoListAdapter = new SimpleAdapter(
                    this.appCompatActivity,
                    itemList,
                    R.layout.order_history_detail_item_row,
                    new String[]{OH_ITEM_QUANTITY, OH_ITEM_CHARGED_PRICE},
                    new int[]{R.id.oh_det_item_description, R.id.oh_det_item_price}
            );
            for (int i = 0; i < itemInfoListAdapter.getCount(); i++) {
                this.itemDetailLayout.addView(itemInfoListAdapter.getView(i, null, null));
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    private void initiateCheckout() {
        try {
            this.progressBar.setVisibility(View.VISIBLE);
            JSONArray jsonArray = this.createJSONFromList(this.clientDB.getOrderHistoryItemsFromDB());
            HashMap<String, Object> checkoutItems = new HashMap<>();
            checkoutItems.put(ITEMS_TO_CHECKOUT, jsonArray);
            checkoutItems.put(EMAIL, this.clientDB.getUserEmail());
            checkoutItems.put(OH_DELIVERY_CHARGES, this.deliverCharges);
            checkoutItems.put(ADDRESS_PARAM, this.addressForCheckout);
            this.checkoutPageController.sendCheckoutItems(checkoutItems);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    private JSONArray createJSONFromList(List<CartItemDataObject> cartItemDataObjects) throws Exception {
        JSONArray jsonArray = new JSONArray();
        for (CartItemDataObject cartItemDataObject : cartItemDataObjects) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ITEM_NAME, cartItemDataObject.getItemName());
            jsonObject.put(ITEM_WEIGHT_OR_QUANTITY, cartItemDataObject.getItemWeightOrQuantity());
            jsonObject.put(ITEM_ID, cartItemDataObject.getItemId());
            jsonObject.put(ITEM_QUANTITY_TO_CHECKOUT, cartItemDataObject.getQuantityToCheckout());
            jsonObject.put(ITEM_PRICE, cartItemDataObject.getItemPrice());
            jsonObject.put(ITEM_DISCOUNT, cartItemDataObject.getItemDiscount());
            jsonArray.put(jsonObject);
        }
        return jsonArray;
    }

    public void cleanUpAfterCheckout(JSONObject response) {
        try {
            if (SUCCESS == Integer.parseInt(response.getString(STATUS))) {
                this.progressBar.setVisibility(View.GONE);
                this.fadeInLayout(THANKS_FOR_SHOPPING);
                PostCheckoutCleanUpTask postCheckoutCleanUpTask = new PostCheckoutCleanUpTask(this.clientDB);
                postCheckoutCleanUpTask.execute();
            } else {
                this.progressBar.setVisibility(View.GONE);
                Toast.makeText(this.appCompatActivity, response.getString(MESSAGE), Toast.LENGTH_LONG).show();
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    public void goBackToStore() {
        try {
            Utilities.getInstance().startNewActivityByFinishingOlder(this.appCompatActivity, StorePage.class);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    public void handleResetPhoneNumberClick() {
        try {
            this.setUpPhoneNumberString();
            this.resetPhoneNumber.setVisibility(View.GONE);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    @Override
    public void startReconnecting() {
        try {
            this.progressBar.setVisibility(View.GONE);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    @Override
    public void tryToReconnect() {
        try {
            this.progressBar.setVisibility(View.VISIBLE);
            LeftNavigationObserver.getLeftNavigationObserverInstance().fetfchLeftNavigation();
            this.fetchPreCheckoutInfo();
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }
}