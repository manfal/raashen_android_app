package com.utilstore.actions;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ProgressBar;

import com.utilstore.analytics.logging.Constants;
import com.utilstore.analytics.logging.ILog;
import com.utilstore.analytics.logging.Logger;
import com.utilstore.app.DepartmentPage;
import com.utilstore.app.R;
import com.utilstore.components.adapter.DepartmentCategoryAdapter;
import com.utilstore.components.dataobjects.DepartmentItemDataObject;
import com.utilstore.components.fragments.DepartmentCategoryFragment;
import com.utilstore.components.interfaces.IInternetConnectionObject;
import com.utilstore.components.layoutmanipulators.ItemDetailLayoutManipulator;
import com.utilstore.components.listeners.DepartmentCategoryPagerListener;
import com.utilstore.components.observers.CartObserver;
import com.utilstore.components.observers.LeftNavigationObserver;
import com.utilstore.datastore.DepartmentController;
import com.utilstore.utilities.InternetConnectionUtility;
import com.utilstore.utilities.Utilities;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import static com.utilstore.constants.Constants.CATEGORY_ID;
import static com.utilstore.constants.Constants.CATEGORY_NAME;
import static com.utilstore.constants.Constants.DEFAULT_CATEGORY_ID;
import static com.utilstore.constants.Constants.DEPARTMENT_CATEGORY_ALL;
import static com.utilstore.constants.Constants.DEPARTMENT_ID;
import static com.utilstore.constants.Constants.DEPARTMENT_NAME;
import static com.utilstore.constants.Constants.META_DATA_NOT_FOUND_EXCEPTION;
import static com.utilstore.constants.Constants.NON_APPLICABLE_ID;

/**
 * Created by anfal on 12/19/2015.
 */
public class DepartmentActions implements IInternetConnectionObject {
    private static DepartmentActions departmentActionInstance = null;
    private ILog logger = null;
    private TabLayout departmentCategories = null;
    private AppCompatActivity appCompatActivity = null;
    private int departmentId = 0;
    private int categoryId = DEFAULT_CATEGORY_ID;
    private DrawerLayout departmentDrawer = null;
    private ViewPager viewPager = null;
    private String departmentName, categoryName, reconnectInDepartmentCategoryName = null;
    private DepartmentCategoryAdapter departmentCategoryAdapter = null;
    private DepartmentController departmentController = null;
    private ItemDetailLayoutManipulator itemDetailLayoutManipulator = null;
    private InternetConnectionUtility internetConnectionUtility = null;
    private boolean isReconnectOutsideDepartment = true;
    private ProgressBar progressBar = null;

    private DepartmentActions(AppCompatActivity appCompatActivity) {
        this.logger = Logger.getLogger();
        try {
            this.appCompatActivity = appCompatActivity;
            Bundle metaData = this.appCompatActivity.getIntent().getExtras();
            if (null == metaData) {
                throw new Exception(META_DATA_NOT_FOUND_EXCEPTION + ": " + DepartmentPage.class);
            } else {
                this.departmentDrawer = (DrawerLayout) this.appCompatActivity.findViewById(R.id.department_drawer_layout);
                Toolbar toolbar = (Toolbar) this.appCompatActivity.findViewById(R.id.department_page_toolbar);
                this.internetConnectionUtility = new InternetConnectionUtility(this.appCompatActivity);
                this.internetConnectionUtility.registerAsObserver(this);
                this.departmentController = new DepartmentController(this.appCompatActivity);
                this.appCompatActivity.setSupportActionBar(toolbar);
                LeftNavigationObserver.setLeftNavigationObserverInstance(this.appCompatActivity, this.departmentDrawer, toolbar);
                this.departmentCategories = (TabLayout) this.appCompatActivity.findViewById(R.id.department_category_tabs);
                this.departmentCategories.setTabMode(TabLayout.MODE_SCROLLABLE);
                Utilities.getInstance().configureTabLook(this.departmentCategories);
                this.setDepartmentId((int) metaData.get(DEPARTMENT_ID));
                this.setDepartmentController(departmentController);
                this.setCategoryId((int) metaData.get(CATEGORY_ID));
                this.setDepartmentName((String) metaData.get(DEPARTMENT_NAME));
                this.viewPager = (ViewPager) this.appCompatActivity.findViewById(R.id.department_category_pager);
                this.progressBar = (ProgressBar) this.appCompatActivity.findViewById(R.id.department_page_progress);
                this.viewPager.addOnPageChangeListener(new DepartmentCategoryPagerListener());
                this.itemDetailLayoutManipulator = new ItemDetailLayoutManipulator(this.appCompatActivity);
                CartObserver.setCartObserverInstance(this.appCompatActivity);
            }

        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public static DepartmentActions getDepartmentActionInstance() {
        return departmentActionInstance;
    }

    public static void setDepartmentActionInstance(AppCompatActivity appCompatActivity) {
        if (null == departmentActionInstance) {
            departmentActionInstance = new DepartmentActions(appCompatActivity);
        }
    }

    public void destroyDepartmentActionInstance() {
        if (null != departmentActionInstance) {
            departmentActionInstance = null;
        }
    }

    public void setDepartmentCategories(JSONArray categoryArray) {
        try {
            DepartmentCategoryAdapter departmentCategoryAdapter = this.populateCategories(categoryArray);
            this.progressBar.setVisibility(View.GONE);
            this.setDepartmentCategoryAdapter(departmentCategoryAdapter);
            this.viewPager.setAdapter(this.getDepartmentCategoryAdapter());
            this.departmentCategories.setupWithViewPager(this.viewPager);
            this.setActionBarDepartmentName(this.departmentName);
            this.selectCurrentlyActiveTab(null);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    private DepartmentCategoryAdapter populateCategories(JSONArray categoryArray) {
        DepartmentCategoryAdapter departmentCategoryAdapter = new DepartmentCategoryAdapter(this.appCompatActivity.getSupportFragmentManager());
        try {
            DepartmentCategoryFragment departmentCategoryFragment = new DepartmentCategoryFragment();
            departmentCategoryFragment.setCategoryId(DEFAULT_CATEGORY_ID);
            departmentCategoryFragment.setDepartmentId(this.getDepartmentId());
            departmentCategoryAdapter.addFragment(departmentCategoryFragment, DEPARTMENT_CATEGORY_ALL);
            for (int categoryNumber = 0; categoryNumber < categoryArray.length(); categoryNumber++) {
                JSONObject categoryObject = categoryArray.getJSONObject(categoryNumber);
                int newCatId = Integer.parseInt(categoryObject.getString(CATEGORY_ID));
                String newCatName = categoryObject.getString(CATEGORY_NAME);
                departmentCategoryFragment = new DepartmentCategoryFragment();
                departmentCategoryFragment.setCategoryId(newCatId);
                departmentCategoryFragment.setDepartmentId(this.getDepartmentId());
                departmentCategoryAdapter.addFragment(departmentCategoryFragment, newCatName);
                this.fillInMissingInfo(newCatName, newCatId);
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
        return departmentCategoryAdapter;
    }

    private void fillInMissingInfo(String newCategoryName, int newCategoryId) throws Exception {
        if (com.utilstore.constants.Constants.checkForStaticData) {
            if (com.utilstore.constants.Constants.categoryName.toLowerCase().equals(newCategoryName.toLowerCase())) {
                this.setCategoryId(newCategoryId);
                this.setCategoryName(newCategoryName);
                com.utilstore.constants.Constants.checkForStaticData = false;
            }
        } else {
            if (null == this.reconnectInDepartmentCategoryName) {
                if (DEFAULT_CATEGORY_ID != this.getCategoryId()) {
                    if (this.getCategoryId() == newCategoryId) {
                        this.setCategoryName(newCategoryName);
                    }
                }
            } else {
                if (this.reconnectInDepartmentCategoryName.toLowerCase().equals(newCategoryName.toLowerCase())) {
                    this.setCategoryName(this.reconnectInDepartmentCategoryName);
                    this.setCategoryId(newCategoryId);
                    this.reconnectInDepartmentCategoryName = null;
                }
            }
        }
    }

    public void selectCurrentlyActiveTab(String inDepartmentCategoryName) {
        List<String> departmentCategoryFragments = this.getDepartmentCategoryAdapter().getCategoryTitles();
        if (null == inDepartmentCategoryName) {
            if (DEFAULT_CATEGORY_ID != this.getCategoryId()) {
                for (int i = 0; i < departmentCategoryFragments.size(); i++) {
                    if (this.getCategoryName().toLowerCase().equals(departmentCategoryFragments.get(i).toLowerCase())) {
                        this.viewPager.setCurrentItem(i);
                    }
                }
            }
        } else {
            for (int i = 0; i < departmentCategoryFragments.size(); i++) {
                if (inDepartmentCategoryName.toLowerCase().equals(departmentCategoryFragments.get(i).toLowerCase())) {
                    this.viewPager.setCurrentItem(i);
                }
            }
        }
        this.getDepartmentDrawer().closeDrawer(Gravity.LEFT);
    }

    public int getDepartmentId() {
        return this.departmentId;
    }

    public void setDepartmentId(int id) {
        this.departmentId = id;
        com.utilstore.constants.Constants.departmentId = id;
    }

    public int getCategoryId() {
        return categoryId;
    }

    private void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
        com.utilstore.constants.Constants.categoryId = categoryId;
    }

    private void setActionBarDepartmentName(String departmentName) {
        if (null != this.appCompatActivity.getSupportActionBar()) {
            this.appCompatActivity.getSupportActionBar().setTitle(departmentName);
        }
    }

    private void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
        com.utilstore.constants.Constants.departmentName = departmentName;
    }

    public DepartmentController getDepartmentController() {
        return this.departmentController;
    }

    public void setDepartmentController(DepartmentController departmentController) {
        this.departmentController = departmentController;
    }

    public void fetchCategoryData(String departmentName, int departmentId, int categoryId) {
        try {
            if (null != departmentName) {
                this.setDepartmentName(departmentName);
            }
            if (NON_APPLICABLE_ID != departmentId) {
                this.setDepartmentId(departmentId);
            }
            if (NON_APPLICABLE_ID != categoryId) {
                this.setCategoryId(categoryId);
            }
            if (this.internetConnectionUtility.checkInternetConnectivity()) {
                this.getDepartmentController().getDepartmentCategories(this.getDepartmentId());
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public DepartmentCategoryAdapter getDepartmentCategoryAdapter() {
        return departmentCategoryAdapter;
    }

    public void setDepartmentCategoryAdapter(DepartmentCategoryAdapter departmentCategoryAdapter) {
        this.departmentCategoryAdapter = departmentCategoryAdapter;
    }

    public DrawerLayout getDepartmentDrawer() {
        return this.departmentDrawer;
    }

    public String getCategoryName() {
        return this.categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public void revealItemDetailLayout(DepartmentItemDataObject departmentItemDataObject, View itemView) {
        try {
            this.itemDetailLayoutManipulator.revealItemDetailLayout(departmentItemDataObject, itemView);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public ItemDetailLayoutManipulator getItemDetailLayoutManipulator() {
        return itemDetailLayoutManipulator;
    }

    @Override
    public void startReconnecting() {
        try {
            this.viewPager.removeAllViews();
            if (this.isReconnectOutsideDepartment) {
                this.departmentCategories.removeAllTabs();
                this.setActionBarDepartmentName(this.appCompatActivity.getString(R.string.text_department));
            } else {
                this.isReconnectOutsideDepartment = true;
            }
            if (this.departmentDrawer.isDrawerOpen(Gravity.LEFT)) {
                this.departmentDrawer.closeDrawer(Gravity.LEFT);
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void tryToReconnect() {
        try {
            this.fetchCategoryData(this.departmentName, this.getDepartmentId(), this.getCategoryId());
            LeftNavigationObserver.getLeftNavigationObserverInstance().fetfchLeftNavigation();
            CartObserver.getCartObserverInstance().instantiateCart();
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void checkInternetConnectivityAgain(int position) {
        try {
            String categoryName = this.departmentCategories.getTabAt(position).getText().toString();
            com.utilstore.constants.Constants.categoryName = categoryName;
            if (Utilities.getInstance().isInternetConnectionNotAvailable(this.appCompatActivity)) {
                this.isReconnectOutsideDepartment = false;
            }
            if (!this.internetConnectionUtility.checkInternetConnectivity()) {
                com.utilstore.constants.Constants.checkForStaticData = false;
                this.reconnectInDepartmentCategoryName = categoryName;
            } else {
                com.utilstore.constants.Constants.checkForStaticData = true;
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }
}
