package com.utilstore.actions;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.flurry.android.FlurryAgent;
import com.utilstore.analytics.LogEvents;
import com.utilstore.analytics.logging.Constants;
import com.utilstore.analytics.logging.ILog;
import com.utilstore.analytics.logging.Logger;
import com.utilstore.app.BuildConfig;
import com.utilstore.app.R;
import com.utilstore.components.CustomComponents;
import com.utilstore.components.adapter.StoreDepartmentsAdapter;
import com.utilstore.components.dataobjects.StoreDepartmentDataObject;
import com.utilstore.components.interfaces.IInternetConnectionObject;
import com.utilstore.components.navigation.StoreMenu;
import com.utilstore.components.observers.CartObserver;
import com.utilstore.components.observers.LeftNavigationObserver;
import com.utilstore.datastore.StoreController;
import com.utilstore.utilities.InternetConnectionUtility;
import com.utilstore.utilities.Utilities;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.utilstore.constants.Constants.APP_VERSION_FLAG;
import static com.utilstore.constants.Constants.DEFAULT_CATEGORY_ID;
import static com.utilstore.constants.Constants.DEPARTMENT_ID;
import static com.utilstore.constants.Constants.DEPARTMENT_NAME;
import static com.utilstore.constants.Constants.DEPARTMENT_PICTURE;

/**
 * Created by anfal on 12/14/2015.
 */
public class StoreActions implements IInternetConnectionObject {
    private static StoreActions storeActionInstance = null;
    private ILog logger = null;
    private StoreMenu storeMenu = null;
    private AppCompatActivity appCompatActivity = null;
    private ProgressBar progressBar = null;
    private DrawerLayout storeDrawer = null;
    private StoreController storeController = null;
    private InternetConnectionUtility internetConnectionUtility = null;

    private StoreActions(AppCompatActivity appCompatActivity) {
        logger = Logger.getLogger();
        this.appCompatActivity = appCompatActivity;
        RecyclerView departmentRecyclerView = (RecyclerView) this.appCompatActivity.findViewById(R.id.store_departments);
        this.storeMenu = new StoreMenu(departmentRecyclerView, this.appCompatActivity);
        Toolbar storeToolbar = (Toolbar) this.appCompatActivity.findViewById(R.id.store_page_toolbar);
        this.storeDrawer = (DrawerLayout) this.appCompatActivity.findViewById(R.id.store_drawer_layout);
        this.progressBar = (ProgressBar) this.appCompatActivity.findViewById(R.id.store_department_progress);
        departmentRecyclerView.setVisibility(View.GONE);
        this.progressBar.setVisibility(View.VISIBLE);
        this.internetConnectionUtility = new InternetConnectionUtility(this.appCompatActivity);
        this.internetConnectionUtility.registerAsObserver(this);
        this.storeController = new StoreController(this.appCompatActivity);
        this.appCompatActivity.setSupportActionBar(storeToolbar);
        LeftNavigationObserver.setLeftNavigationObserverInstance(this.appCompatActivity, this.storeDrawer, storeToolbar);
        CartObserver.setCartObserverInstance(this.appCompatActivity);
        Bundle metaData = this.appCompatActivity.getIntent().getExtras();
        if (null != metaData) {
            this.showUpdateAppDialog((int) metaData.get(APP_VERSION_FLAG));
        }
    }

    public static StoreActions getStoreActionInstance() {
        return storeActionInstance;
    }

    public static void setStoreActionInstance(AppCompatActivity appCompatActivity) {
        if (null == storeActionInstance) {
            storeActionInstance = new StoreActions(appCompatActivity);
        }
    }

    public void destroyStoreActionInstance() {
        if (null != storeActionInstance) {
            storeActionInstance = null;
        }
    }

    public void populateStoreDepartments(JSONArray items) {
        try {
            boolean isStoreMenuLoaded = this.storeMenu.instantiateStoreMenu(this.parseDepartmentNav(items));
            if (isStoreMenuLoaded) {
                this.storeMenu.getRecyclerView().setVisibility(View.VISIBLE);
                this.progressBar.setVisibility(View.GONE);
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    private List<StoreDepartmentDataObject> parseDepartmentNav(JSONArray departmentArray) {
        List<StoreDepartmentDataObject> storeDepartmentDataObjects = new ArrayList<>();
        try {
            for (int departmentNumber = 0; departmentNumber < departmentArray.length(); departmentNumber++) {
                JSONObject departmentObject = departmentArray.getJSONObject(departmentNumber);
                storeDepartmentDataObjects.add(
                        new StoreDepartmentDataObject(
                                departmentObject.getString(DEPARTMENT_NAME),
                                Integer.parseInt(departmentObject.getString(DEPARTMENT_ID)),
                                departmentObject.getString(DEPARTMENT_PICTURE)
                        )
                );
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
        return storeDepartmentDataObjects;
    }

    public void storeDepartmentClickListener(int position) {
        try {
            StoreDepartmentsAdapter storeDepartmentsAdapter = this.storeMenu.getStoreDepartmentsAdapter();
            StoreDepartmentDataObject storeDepartmentDataObject = storeDepartmentsAdapter.getStoreItemList().get(position);
            FlurryAgent.logEvent(
                    LogEvents.EVENT_ID_MAIN_STORE_DEPARTMENT,
                    LogEvents.getEventData(
                            LogEvents.EVENT_ID_MAIN_STORE_DEPARTMENT,
                            new String[]{
                                    LogEvents.TRIGGERED_MAIN_DEPARTMENT_LOAD_EVENT,
                                    storeDepartmentDataObject.getDepartmentName()
                            }
                    )
            );
            Utilities.getInstance().resetStaticActivityData();
            Utilities.getInstance().startDepartmentActivity(
                    this.appCompatActivity,
                    storeDepartmentDataObject.getDepartmentName(),
                    storeDepartmentDataObject.getDepartmentId(),
                    DEFAULT_CATEGORY_ID
            );
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public StoreController getStoreController() {
        return this.storeController;
    }

    public DrawerLayout getStoreDrawer() {
        return this.storeDrawer;
    }

    private void showUpdateAppDialog(int newVersionCode) {
        try {
            if (BuildConfig.VERSION_CODE < newVersionCode) {
                CustomComponents.getInstance().showNewVersionIsAvailableDialog(this.appCompatActivity).show();
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void fetchStoreDepartments() {
        try {
            if (this.internetConnectionUtility.checkInternetConnectivity()) {
                this.getStoreController().getStoreDepartments();
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void startReconnecting() {
        try {
            this.progressBar.setVisibility(View.GONE);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void tryToReconnect() {
        try {
            this.progressBar.setVisibility(View.VISIBLE);
            this.fetchStoreDepartments();
            LeftNavigationObserver.getLeftNavigationObserverInstance().fetfchLeftNavigation();
            CartObserver.getCartObserverInstance().instantiateCart();
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }
}