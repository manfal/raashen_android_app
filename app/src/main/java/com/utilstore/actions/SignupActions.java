package com.utilstore.actions;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import com.utilstore.analytics.logging.Constants;
import com.utilstore.analytics.logging.ILog;
import com.utilstore.analytics.logging.Logger;
import com.utilstore.app.CheckoutPage;
import com.utilstore.app.LoginPage;
import com.utilstore.app.R;
import com.utilstore.app.StorePage;
import com.utilstore.components.CustomComponents;
import com.utilstore.components.interfaces.IInternetConnectionObject;
import com.utilstore.components.listeners.LoginLinkListener;
import com.utilstore.components.listeners.SignupButtonListener;
import com.utilstore.datastore.SignupController;
import com.utilstore.utilities.InternetConnectionUtility;
import com.utilstore.utilities.Utilities;
import com.utilstore.utilities.Validators;

import org.json.JSONObject;

import java.util.HashMap;

import static com.utilstore.constants.Constants.CHARGEDPRICE;
import static com.utilstore.constants.Constants.EMAIL;
import static com.utilstore.constants.Constants.EMAIL_PARAM;
import static com.utilstore.constants.Constants.FIRSTNAME_PARAM;
import static com.utilstore.constants.Constants.LASTNAME_PARAM;
import static com.utilstore.constants.Constants.MESSAGE;
import static com.utilstore.constants.Constants.PASSWORD_PARAM;
import static com.utilstore.constants.Constants.ROLE;
import static com.utilstore.constants.Constants.STATUS;
import static com.utilstore.constants.Constants.SUCCESS;
import static com.utilstore.constants.Constants.THRESHOLDPRICE;
import static com.utilstore.constants.Constants.shouldUserBeRedirectedToCheckoutPage;

/**
 * Created by anfal on 12/26/2015.
 */
public class SignupActions implements IInternetConnectionObject {

    private static SignupActions signupActions = null;
    private AppCompatActivity appCompatActivity = null;
    private ProgressDialog progressDialog = null;
    private SignupController signupController = null;
    private ILog logger = null;
    private EditText firstName, lastName, email, password = null;
    private InternetConnectionUtility internetConnectionUtility = null;

    private SignupActions(AppCompatActivity appCompatActivity) {
        this.logger = Logger.getLogger();
        try {
            this.appCompatActivity = appCompatActivity;
            this.signupController = new SignupController(this.appCompatActivity);
            Button loginButton = (Button) this.appCompatActivity.findViewById(R.id.login_button);
            Button signupButton = (Button) this.appCompatActivity.findViewById(R.id.signup_button);
            this.firstName = (EditText) this.appCompatActivity.findViewById(R.id.signup_fname);
            this.lastName = (EditText) this.appCompatActivity.findViewById(R.id.signup_lname);
            this.email = (EditText) this.appCompatActivity.findViewById(R.id.signup_email);
            this.password = (EditText) this.appCompatActivity.findViewById(R.id.signup_password);
            loginButton.setOnClickListener(new LoginLinkListener());
            signupButton.setOnClickListener(new SignupButtonListener());
            this.internetConnectionUtility = new InternetConnectionUtility(this.appCompatActivity);
            this.internetConnectionUtility.registerAsObserver(this);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public static SignupActions getSignupActionsInstance() {
        return signupActions;
    }

    public static void setSignupActionsInstance(AppCompatActivity appCompatActivity) {
        if (null == signupActions) {
            signupActions = new SignupActions(appCompatActivity);
        }
    }

    public void destroySignupActionsInstance() {
        if (null != signupActions) {
            signupActions = null;
        }
    }

    public void signUp() {
        try {
            if (this.internetConnectionUtility.checkInternetConnectivity()) {
                String firstName = this.firstName.getText().toString();
                String lastName = this.lastName.getText().toString();
                String email = this.email.getText().toString();
                String password = this.password.getText().toString();

                if (firstName.isEmpty() || lastName.isEmpty() || email.isEmpty() || password.isEmpty()) {
                    CustomComponents.getInstance().messageDialog(this.appCompatActivity, this.appCompatActivity.getString(R.string.text_error_prompt), this.appCompatActivity.getString(R.string.alert_incomplete_information)).show();
                } else if (!Validators.getInstance().isEmailValid(email)) {
                    CustomComponents.getInstance().messageDialog(this.appCompatActivity, this.appCompatActivity.getString(R.string.text_error_prompt), this.appCompatActivity.getString(R.string.email_not_valid)).show();
                } else {
                    final HashMap<String, String> signupParams = new HashMap<>();
                    signupParams.put(FIRSTNAME_PARAM, firstName);
                    signupParams.put(LASTNAME_PARAM, lastName);
                    signupParams.put(EMAIL_PARAM, email);
                    signupParams.put(PASSWORD_PARAM, password);
                    this.progressDialog = ProgressDialog.show(this.appCompatActivity, this.appCompatActivity.getString(R.string.text_signingup_prompt), this.appCompatActivity.getString(R.string.text_please_wait), true);
                    this.signupController.signUpUser(signupParams);
                }
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void userRegistered(JSONObject response) {
        try {
            if (SUCCESS == Integer.parseInt(response.getString(STATUS))) {
                Utilities.getInstance().insertSessionRecordAfterDeletionInDB(
                        this.appCompatActivity,
                        response.getString(ROLE),
                        response.getString(EMAIL),
                        Integer.parseInt(response.getString(THRESHOLDPRICE)),
                        Integer.parseInt(response.getString(CHARGEDPRICE))
                );
                this.progressDialog.dismiss();
                if (shouldUserBeRedirectedToCheckoutPage) {
                    shouldUserBeRedirectedToCheckoutPage = false;
                    Utilities.getInstance().startNewActivityByFinishingOlder(this.appCompatActivity, CheckoutPage.class);
                } else {
                    Utilities.getInstance().startNewActivityByFinishingOlder(this.appCompatActivity, StorePage.class);
                }
            } else {
                this.progressDialog.dismiss();
                CustomComponents.getInstance().messageDialog(this.appCompatActivity, this.appCompatActivity.getString(R.string.text_error_prompt), response.getString(MESSAGE)).show();
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void startLoginActivity() {
        try {
            Utilities.getInstance().startNewActivityByFinishingOlder(this.appCompatActivity, LoginPage.class);
        } catch (Exception ex) {
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void startReconnecting() {
        this.internetConnectionUtility.showNoInternetMessage();
    }

    @Override
    public void tryToReconnect() {
        try {
            this.signUp();
        } catch (Exception ex) {
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }
}
