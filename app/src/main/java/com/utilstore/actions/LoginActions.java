package com.utilstore.actions;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.utilstore.analytics.logging.Constants;
import com.utilstore.analytics.logging.ILog;
import com.utilstore.analytics.logging.Logger;
import com.utilstore.app.CheckoutPage;
import com.utilstore.app.R;
import com.utilstore.app.ResetPasswordPage;
import com.utilstore.app.SignupPage;
import com.utilstore.app.StorePage;
import com.utilstore.components.CustomComponents;
import com.utilstore.components.interfaces.IInternetConnectionObject;
import com.utilstore.components.listeners.ForgotPasswordLinkListener;
import com.utilstore.components.listeners.LoginButtonClickListener;
import com.utilstore.components.listeners.SignupLinkListener;
import com.utilstore.datastore.LoginController;
import com.utilstore.utilities.InternetConnectionUtility;
import com.utilstore.utilities.Utilities;
import com.utilstore.utilities.Validators;

import org.json.JSONObject;

import java.util.HashMap;

import static com.utilstore.constants.Constants.CHARGEDPRICE;
import static com.utilstore.constants.Constants.EMAIL;
import static com.utilstore.constants.Constants.MESSAGE;
import static com.utilstore.constants.Constants.PASSWORD_PARAM;
import static com.utilstore.constants.Constants.ROLE;
import static com.utilstore.constants.Constants.STATUS;
import static com.utilstore.constants.Constants.SUCCESS;
import static com.utilstore.constants.Constants.THRESHOLDPRICE;
import static com.utilstore.constants.Constants.USERNAME_PARAM;
import static com.utilstore.constants.Constants.shouldUserBeRedirectedToCheckoutPage;

/**
 * Created by anfal on 12/25/2015.
 */
public class LoginActions implements IInternetConnectionObject {

    private static LoginActions loginActionsInstance = null;
    private AppCompatActivity appCompatActivity = null;
    private EditText email, password = null;
    private LoginController loginController = null;
    private ILog logger = null;
    private ProgressDialog progressDialog = null;
    private InternetConnectionUtility internetConnectionUtility = null;

    private LoginActions(AppCompatActivity appCompatActivity) {
        this.logger = Logger.getLogger();
        try {
            this.appCompatActivity = appCompatActivity;
            this.loginController = new LoginController(this.appCompatActivity);
            TextView loginForgotPasswordLink = (TextView) this.appCompatActivity.findViewById(R.id.login_forgot_password_link);
            Button signupButton = (Button) this.appCompatActivity.findViewById(R.id.signup_button);
            Button loginButton = (Button) this.appCompatActivity.findViewById(R.id.login_button);
            this.email = (EditText) this.appCompatActivity.findViewById(R.id.login_email);
            this.password = (EditText) this.appCompatActivity.findViewById(R.id.login_password);
            Utilities.getInstance().decorateHyperLink(loginForgotPasswordLink);
            loginForgotPasswordLink.setOnClickListener(new ForgotPasswordLinkListener());
            signupButton.setOnClickListener(new SignupLinkListener());
            loginButton.setOnClickListener(new LoginButtonClickListener());
            this.internetConnectionUtility = new InternetConnectionUtility(this.appCompatActivity);
            this.internetConnectionUtility.registerAsObserver(this);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public static LoginActions getLoginActionsInstance() {
        return loginActionsInstance;
    }

    public static void setLoginActionsInstance(AppCompatActivity appCompatActivity) {
        if (null == loginActionsInstance) {
            loginActionsInstance = new LoginActions(appCompatActivity);
        }
    }

    public void destroyLoginActionInstance() {
        if (null != loginActionsInstance) {
            loginActionsInstance = null;
        }
    }

    public void validateUserInfo() {
        try {
            if (this.internetConnectionUtility.checkInternetConnectivity()) {
                String email = this.email.getText().toString();
                String password = this.password.getText().toString();
                if (email.isEmpty() || password.isEmpty()) {
                    CustomComponents.getInstance().messageDialog(this.appCompatActivity, this.appCompatActivity.getString(R.string.text_error_prompt), this.appCompatActivity.getString(R.string.alert_incomplete_information)).show();
                } else if (!Validators.getInstance().isEmailValid(email)) {
                    CustomComponents.getInstance().messageDialog(this.appCompatActivity, this.appCompatActivity.getString(R.string.text_error_prompt), this.appCompatActivity.getString(R.string.email_not_valid)).show();
                } else {
                    HashMap<String, String> loginParams = new HashMap<>();
                    loginParams.put(USERNAME_PARAM, email);
                    loginParams.put(PASSWORD_PARAM, password);
                    this.progressDialog = ProgressDialog.show(this.appCompatActivity, this.appCompatActivity.getString(R.string.text_signing_in), this.appCompatActivity.getString(R.string.text_please_wait), true);
                    this.loginController.validateUserLoginInfo(loginParams);
                }
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void loginUser(JSONObject response) {
        try {
            if (SUCCESS == Integer.parseInt(response.getString(STATUS))) {
                Utilities.getInstance().insertSessionRecordAfterDeletionInDB(
                        this.appCompatActivity,
                        response.getString(ROLE),
                        response.getString(EMAIL),
                        Integer.parseInt(response.getString(THRESHOLDPRICE)),
                        Integer.parseInt(response.getString(CHARGEDPRICE))
                );
                this.progressDialog.dismiss();
                if (shouldUserBeRedirectedToCheckoutPage) {
                    shouldUserBeRedirectedToCheckoutPage = false;
                    Utilities.getInstance().startNewActivityByFinishingOlder(this.appCompatActivity, CheckoutPage.class);
                } else {
                    Utilities.getInstance().startNewActivityByFinishingOlder(this.appCompatActivity, StorePage.class);
                }
            } else {
                this.progressDialog.dismiss();
                CustomComponents.getInstance().messageDialog(this.appCompatActivity, this.appCompatActivity.getString(R.string.text_error_prompt), response.getString(MESSAGE)).show();
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void startResetPasswordActivity() {
        try {
            Utilities.getInstance().startNewActivityByFinishingOlder(this.appCompatActivity, ResetPasswordPage.class);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void startSignupActivity() {
        try {
            Utilities.getInstance().startNewActivityByFinishingOlder(this.appCompatActivity, SignupPage.class);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void startReconnecting() {
        this.internetConnectionUtility.showNoInternetMessage();
    }

    @Override
    public void tryToReconnect() {
        try {
            this.validateUserInfo();
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }
}