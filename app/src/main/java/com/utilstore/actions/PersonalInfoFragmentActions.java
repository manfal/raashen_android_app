package com.utilstore.actions;

import android.app.Dialog;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.utilstore.analytics.LogEvents;
import com.utilstore.analytics.logging.Constants;
import com.utilstore.analytics.logging.ILog;
import com.utilstore.analytics.logging.Logger;
import com.utilstore.app.R;
import com.utilstore.components.CustomComponents;
import com.utilstore.components.interfaces.IPhoneNumberVerificationInterface;
import com.utilstore.components.listeners.MyAccountEditPasswordListener;
import com.utilstore.components.listeners.MyAccountEditPersonalInfoListener;
import com.utilstore.components.listeners.MyAccountVerifyPhoneNumberListener;
import com.utilstore.database.ClientDB;
import com.utilstore.datastore.PersonalInfoFragmentController;
import com.utilstore.utilities.Utilities;

import org.json.JSONObject;

import java.util.HashMap;

import static com.utilstore.constants.Constants.EMAIL;
import static com.utilstore.constants.Constants.EMAIL_PARAM;
import static com.utilstore.constants.Constants.ERROR;
import static com.utilstore.constants.Constants.FIRSTNAME_PARAM;
import static com.utilstore.constants.Constants.FIRST_NAME;
import static com.utilstore.constants.Constants.IS_MOBILE_NUMBER_CONFIRMED;
import static com.utilstore.constants.Constants.LASTNAME_PARAM;
import static com.utilstore.constants.Constants.LAST_NAME;
import static com.utilstore.constants.Constants.MESSAGE;
import static com.utilstore.constants.Constants.MOBILE_NUMBER;
import static com.utilstore.constants.Constants.MOBILE_NUMBER_PARAM;
import static com.utilstore.constants.Constants.NEW_PASSWORD_PARAM;
import static com.utilstore.constants.Constants.OLD_PASSWORD_PARAM;
import static com.utilstore.constants.Constants.REPEAT_NEW_PASSWORD_PARAM;
import static com.utilstore.constants.Constants.STATUS;
import static com.utilstore.constants.Constants.SUCCESS;
import static com.utilstore.constants.Constants.USER_INFO;

/**
 * Created by anfal on 1/2/2016.
 */
public class PersonalInfoFragmentActions implements IPhoneNumberVerificationInterface {

    private static PersonalInfoFragmentActions personalInfoActionInstance = null;
    private Context context = null;
    private ILog logger = null;
    private TextView email, firstName, lastName, phNum, verifyNumberHlink = null;
    private ProgressBar progressBar = null;
    private PersonalInfoFragmentController personalInfoFragmentController = null;
    private Dialog editPasswordDialog;
    private FrameLayout personalInfoLayout = null;
    private LinearLayout personalInfoEditLayout = null;
    private ScrollView personalInfoCardScroll = null;

    private PersonalInfoFragmentActions(View view) {
        this.logger = Logger.getLogger();
        try {
            this.context = view.getContext();
            this.email = (TextView) view.findViewById(R.id.account_info_email_text);
            this.firstName = (TextView) view.findViewById(R.id.account_personal_info_fname_text);
            this.lastName = (TextView) view.findViewById(R.id.account_personal_info_lname_text);
            this.phNum = (TextView) view.findViewById(R.id.account_personal_info_phnum_text);
            this.progressBar = (ProgressBar) view.findViewById(R.id.pesonal_info_progress);
            this.verifyNumberHlink = (TextView) view.findViewById(R.id.verify_number_hyperlink);
            this.personalInfoLayout = (FrameLayout) view.findViewById(R.id.personal_info_layout);
            this.personalInfoEditLayout = (LinearLayout) view.findViewById(R.id.edit_personal_info_layout);
            this.personalInfoCardScroll = (ScrollView) view.findViewById(R.id.personal_info_card_scroll);
            TextView editPasswordLink = (TextView) view.findViewById(R.id.edit_password_hyperlink);
            TextView editPersonalInfoLink = (TextView) view.findViewById(R.id.edit_personal_info_hyperlink);
            Utilities.getInstance().decorateHyperLink(editPasswordLink);
            Utilities.getInstance().decorateHyperLink(editPersonalInfoLink);
            editPasswordLink.setOnClickListener(new MyAccountEditPasswordListener());
            editPersonalInfoLink.setOnClickListener(new MyAccountEditPersonalInfoListener());
            this.personalInfoFragmentController = new PersonalInfoFragmentController(this.context);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    public static PersonalInfoFragmentActions getPersonalInfoActionInstance() {
        return personalInfoActionInstance;
    }

    public static void setPersonalInfoActionInstance(View view) {
        if (null == personalInfoActionInstance) {
            personalInfoActionInstance = new PersonalInfoFragmentActions(view);
        }
    }

    public void destroyPersonalInfoActionInstance() {
        if (null != personalInfoActionInstance) {
            personalInfoActionInstance = null;
        }
    }

    public PersonalInfoFragmentController getPersonalInfoFragmentController() {
        return personalInfoFragmentController;
    }

    public void startUserAccountInfoRetrieval() {
        try {
            String email = new ClientDB(this.context).getUserEmail();
            this.getPersonalInfoFragmentController().getUserAccountInfo(email);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    public void displayUserAccountInfo(JSONObject jsonObject) {
        try {
            this.email.setText(jsonObject.getString(EMAIL));
            this.populatePersonalInfoCard(
                    jsonObject.getString(FIRST_NAME),
                    jsonObject.getString(LAST_NAME),
                    jsonObject.getString(MOBILE_NUMBER),
                    Integer.parseInt(jsonObject.getString(IS_MOBILE_NUMBER_CONFIRMED))
            );
            this.progressBar.setVisibility(View.GONE);
            this.personalInfoCardScroll.setVisibility(View.VISIBLE);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    public void handlePasswordEditHyperLinkClick() {
        try {
            this.editPasswordDialog = CustomComponents.getInstance().userPasswordEditDialog(this.context).show();
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    public void handlePasswordEdit() {
        try {
            EditText newPassword = (EditText) this.editPasswordDialog.findViewById(R.id.new_password);
            EditText repeatNewPassword = (EditText) this.editPasswordDialog.findViewById(R.id.repeat_new_password);
            EditText oldPassword = (EditText) this.editPasswordDialog.findViewById(R.id.old_password);

            if (!newPassword.getText().toString().equals("") && !repeatNewPassword.getText().toString().equals("") && !oldPassword.getText().toString().equals("")) {
                FlurryAgent.logEvent(
                        LogEvents.EVENT_ID_PASSWORD_RESET,
                        LogEvents.getEventData(
                                LogEvents.EVENT_ID_PASSWORD_RESET,
                                new String[]{
                                        LogEvents.TRIGGERED_RESET_BUTTON_MY_ACCOUNT_CLICK_EVENT
                                }
                        )
                );
                this.progressBar.setVisibility(View.VISIBLE);
                HashMap<String, String> editPasswordParams = new HashMap<>();
                String email = new ClientDB(this.context).getUserEmail();
                editPasswordParams.put(EMAIL_PARAM, email);
                editPasswordParams.put(NEW_PASSWORD_PARAM, newPassword.getText().toString());
                editPasswordParams.put(REPEAT_NEW_PASSWORD_PARAM, repeatNewPassword.getText().toString());
                editPasswordParams.put(OLD_PASSWORD_PARAM, oldPassword.getText().toString());
                this.getPersonalInfoFragmentController().makeEditPasswordRequest(editPasswordParams);
            }

        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    public void postPasswordEditChores(JSONObject response) {
        try {
            this.progressBar.setVisibility(View.GONE);
            Snackbar.make(this.personalInfoLayout, response.getString(MESSAGE), Snackbar.LENGTH_LONG).show();
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    public void handlePersonalInfoEditHyperLinkClick() {
        try {
            String defaultPhNumText = this.context.getString(R.string.account_personal_info_phnum_default_text);
            String firstName = this.firstName.getText().toString();
            String lastName = this.lastName.getText().toString();
            String phNum = "";
            if (!this.phNum.getText().toString().equals(defaultPhNumText)) {
                phNum = this.phNum.getText().toString();
            }
            CustomComponents.getInstance().userPersonalInfoEditDialog(
                    this.context,
                    this.personalInfoEditLayout,
                    firstName,
                    lastName,
                    phNum
            ).show();
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    public void handlePersonalInfoEdit(String firstName, String lastName, String phoneNumber) {
        try {
            if (!phoneNumber.equals("") && !firstName.equals("") && !lastName.equals("")) {
                FlurryAgent.logEvent(
                        LogEvents.EVENT_ID_PERSONAL_INFO_EDIT,
                        LogEvents.getEventData(
                                LogEvents.EVENT_ID_PERSONAL_INFO_EDIT,
                                new String[]{
                                        LogEvents.TRIGGERED_PERSONAL_INFO_EDIT
                                }
                        )
                );
                this.progressBar.setVisibility(View.VISIBLE);
                HashMap<String, String> personalInfoParams = new HashMap<>();
                String email = new ClientDB(this.context).getUserEmail();
                personalInfoParams.put(FIRSTNAME_PARAM, firstName);
                personalInfoParams.put(LASTNAME_PARAM, lastName);
                personalInfoParams.put(MOBILE_NUMBER_PARAM, phoneNumber);
                personalInfoParams.put(EMAIL_PARAM, email);
                this.getPersonalInfoFragmentController().makeEditPersonalInfoRequest(personalInfoParams);
            } else {
                Snackbar.make(this.personalInfoLayout, R.string.incomplete_info_cannot_proceed, Snackbar.LENGTH_LONG).show();
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    @Override
    public void handleVerifyPhoneNumberLinkClick() {
        try {
            CustomComponents.getInstance().verifyPhoneNumberDialog(this.context, this);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    @Override
    public void handlePhoneNumberVerification(String verificationCode) {
        try {
            if (!verificationCode.equals("")) {
                this.progressBar.setVisibility(View.VISIBLE);
                FlurryAgent.logEvent(
                        LogEvents.EVENT_ID_NUMBER_VERIFICATION,
                        LogEvents.getEventData(
                                LogEvents.EVENT_ID_NUMBER_VERIFICATION,
                                new String[]{
                                        LogEvents.TRIGGERED_NUMBER_VERIFICATION_MY_ACCOUNT
                                }
                        )
                );
                String email = new ClientDB(this.context).getUserEmail();
                PhoneNumberVerificationAction phoneNumberVerificationAction = new PhoneNumberVerificationAction();
                phoneNumberVerificationAction.sendVerficationCodeConfirmationRequest(this.context, this, verificationCode, email);
            } else {
                Snackbar.make(this.personalInfoLayout, R.string.incomplete_info_cannot_proceed, Snackbar.LENGTH_LONG).show();
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    @Override
    public void postVerificationChores(JSONObject jsonObject) {
        try {
            this.progressBar.setVisibility(View.GONE);
            if (SUCCESS == Integer.parseInt(jsonObject.getString(STATUS))) {
                this.verifyNumberHlink.setVisibility(View.GONE);
            }
            Snackbar.make(this.personalInfoLayout, jsonObject.getString(MESSAGE), Snackbar.LENGTH_LONG).show();
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    public void postPersonalInfoEditChores(JSONObject personalInfoResult) {
        try {
            this.progressBar.setVisibility(View.GONE);
            if (SUCCESS == Integer.parseInt(personalInfoResult.getString(STATUS))) {
                JSONObject userInfo = personalInfoResult.getJSONObject(USER_INFO);
                this.populatePersonalInfoCard(
                        userInfo.getString(FIRST_NAME),
                        userInfo.getString(LAST_NAME),
                        userInfo.getString(MOBILE_NUMBER),
                        Integer.parseInt(userInfo.getString(IS_MOBILE_NUMBER_CONFIRMED))
                );
            }
            Snackbar.make(this.personalInfoLayout, personalInfoResult.getString(MESSAGE), Snackbar.LENGTH_LONG).show();
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    private void populatePersonalInfoCard(String firstName, String lastName, String phoneNumber, int mobileNumberConfirmed) throws Exception {
        this.firstName.setText(firstName);
        this.lastName.setText(lastName);
        if (!phoneNumber.equals("") && !phoneNumber.equals("null")) {
            this.phNum.setText(phoneNumber);
            if (ERROR == mobileNumberConfirmed) {
                Utilities.getInstance().decorateHyperLink(this.verifyNumberHlink);
                this.verifyNumberHlink.setOnClickListener(new MyAccountVerifyPhoneNumberListener(this));
                this.verifyNumberHlink.setVisibility(View.VISIBLE);
            }
        }
    }
}