package com.utilstore.actions;

import android.content.Context;
import android.util.Log;

import com.utilstore.analytics.logging.ILog;
import com.utilstore.analytics.logging.Logger;
import com.utilstore.components.interfaces.IAddressResponseSenderReceiver;
import com.utilstore.datastore.NewAddressRequestController;

import java.util.HashMap;

import static com.utilstore.analytics.logging.Constants.ERROR;
import static com.utilstore.analytics.logging.Constants.TAG_ERROR;
import static com.utilstore.constants.Constants.ADDRESS_PARAM;
import static com.utilstore.constants.Constants.CITY_PARAM;
import static com.utilstore.constants.Constants.EMAIL_PARAM;
import static com.utilstore.constants.Constants.LABEL_PARAM;

/**
 * Created by anfal on 1/28/2016.
 */
public class NewAddressRequestAction {

    private ILog logger = null;

    public NewAddressRequestAction() {
        this.logger = Logger.getLogger();
    }

    public void sendNewAddressRequest(Context context, IAddressResponseSenderReceiver addressResponseSenderReceiver, String label, String address, String city, String email) {
        try {
            HashMap<String, String> newAddress = new HashMap<>();
            newAddress.put(LABEL_PARAM, label);
            newAddress.put(ADDRESS_PARAM, address);
            newAddress.put(CITY_PARAM, city);
            newAddress.put(EMAIL_PARAM, email);
            NewAddressRequestController newAddressRequestController = new NewAddressRequestController(context);
            newAddressRequestController.makeNewAddressRequest(newAddress, addressResponseSenderReceiver);
        } catch (Exception ex) {
            this.logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

}
