package com.utilstore.actions;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.utilstore.analytics.logging.Constants;
import com.utilstore.analytics.logging.ILog;
import com.utilstore.analytics.logging.Logger;
import com.utilstore.app.CustomerHelpPage;
import com.utilstore.app.DepartmentPage;
import com.utilstore.app.R;
import com.utilstore.components.dataobjects.DepartmentItemDataObject;
import com.utilstore.components.interfaces.IInternetConnectionObject;
import com.utilstore.components.layoutmanipulators.ItemDetailLayoutManipulator;
import com.utilstore.components.listeners.ActionBarBackIconListener;
import com.utilstore.components.listeners.SearchItemActionListener;
import com.utilstore.components.listeners.SearchItemsScrollListener;
import com.utilstore.components.navigation.DepartmentItems;
import com.utilstore.components.observers.CartObserver;
import com.utilstore.database.ClientDB;
import com.utilstore.datastore.SearchController;
import com.utilstore.enumerations.EItemFetchChoice;
import com.utilstore.utilities.InternetConnectionUtility;
import com.utilstore.utilities.Utilities;

import org.json.JSONArray;

import java.util.List;

import static com.utilstore.constants.Constants.categoryId;
import static com.utilstore.constants.Constants.className;
import static com.utilstore.constants.Constants.departmentId;
import static com.utilstore.constants.Constants.departmentName;
import static com.utilstore.constants.Constants.pageName;

/**
 * Created by anfal on 12/31/2015.
 */
public class SearchActions implements IInternetConnectionObject {

    private static SearchActions searchActionsInstance = null;
    private AppCompatActivity appCompatActivity = null;
    private ILog logger = null;
    private EditText searchEditText = null;
    private RecyclerView searchPageItems = null;
    private ProgressBar searchProgressBar = null;
    private SearchController searchController = null;
    private boolean isDataFetchNecessary;
    private DepartmentItems departmentItems = null;
    private int itemBatch = 0;
    private String searchKeyword = null;
    private DrawerLayout searchItemPageLayout = null;
    private TextView noSearchResults = null;
    private ItemDetailLayoutManipulator itemDetailLayoutManipulator = null;
    private InternetConnectionUtility internetConnectionUtility = null;

    private SearchActions(AppCompatActivity appCompatActivity) {
        this.logger = Logger.getLogger();
        try {
            this.appCompatActivity = appCompatActivity;
            Toolbar searchToolbar = (Toolbar) this.appCompatActivity.findViewById(R.id.searchpage_toolbar);
            this.appCompatActivity.setSupportActionBar(searchToolbar);
            if (null != this.appCompatActivity.getSupportActionBar()) {
                this.appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
            searchToolbar.setNavigationOnClickListener(new ActionBarBackIconListener());
            this.searchEditText = (EditText) this.appCompatActivity.findViewById(R.id.search_view);
            this.searchPageItems = (RecyclerView) this.appCompatActivity.findViewById(R.id.search_page_items);
            this.getSearchPageItems().addOnScrollListener(new SearchItemsScrollListener());
            this.searchProgressBar = (ProgressBar) this.appCompatActivity.findViewById(R.id.search_items_progress);
            this.searchController = new SearchController(this.appCompatActivity);
            this.internetConnectionUtility = new InternetConnectionUtility(this.appCompatActivity);
            this.internetConnectionUtility.registerAsObserver(this);
            this.isDataFetchNecessary = true;
            this.departmentItems = new DepartmentItems(this.getSearchPageItems(), this.appCompatActivity);
            this.searchItemPageLayout = (DrawerLayout) this.appCompatActivity.findViewById(R.id.search_drawer_layout);
            this.noSearchResults = (TextView) this.appCompatActivity.findViewById(R.id.dont_have_items);
            this.getSearchEditText().setOnEditorActionListener(new SearchItemActionListener());
            this.itemDetailLayoutManipulator = new ItemDetailLayoutManipulator(this.appCompatActivity);
            CartObserver.setCartObserverInstance(this.appCompatActivity);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public static SearchActions getSearchActionsInstance() {
        return searchActionsInstance;
    }

    public static void setSearchActionsInstance(AppCompatActivity appCompatActivity) {
        if (null == searchActionsInstance) {
            searchActionsInstance = new SearchActions(appCompatActivity);
        }
    }

    public void destroySearchActionsInstance() {
        if (null != searchActionsInstance) {
            searchActionsInstance = null;
        }
    }

    public void getSearchItems(String searchKeyword) {
        try {
            this.resetItemBatch();
            this.searchKeyword = searchKeyword;
            this.getSearchPageItems().setVisibility(View.GONE);
            this.getNoSearchResults().setVisibility(View.GONE);
            this.fetchSearchItems(EItemFetchChoice.FETCHING_FIRST_TIME);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void setSearchedItems(JSONArray itemsArray) {
        try {
            List<DepartmentItemDataObject> dataSet = Utilities.getInstance().parseItems(itemsArray);
            this.getSearchProgressBar().setVisibility(View.GONE);
            Utilities.getInstance().hideSoftKeyBoard(
                    this.appCompatActivity,
                    this.appCompatActivity.getCurrentFocus()
            );
            if (0 != dataSet.size()) {
                this.isDataFetchNecessary = Utilities.getInstance().setIsDataFetchNecessary(dataSet.size());
                boolean areItemsLoaded = this.getDepartmentItems().instantiateDepartmentItems(dataSet);
                if (areItemsLoaded) {
                    this.getSearchPageItems().setVisibility(View.VISIBLE);
                    this.itemBatch = Utilities.getInstance().incrementItemBatch(this.itemBatch);
                }
            } else {
                this.getNoSearchResults().setVisibility(View.VISIBLE);
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public EditText getSearchEditText() {
        return searchEditText;
    }

    public RecyclerView getSearchPageItems() {
        return searchPageItems;
    }

    public SearchController getSearchController() {
        return searchController;
    }

    public ProgressBar getSearchProgressBar() {
        return searchProgressBar;
    }

    public boolean isDataFetchNecessary() {
        return isDataFetchNecessary;
    }

    public DepartmentItems getDepartmentItems() {
        return departmentItems;
    }

    public void fetchSearchItems(EItemFetchChoice fetchChoice) {
        try {
            if (this.internetConnectionUtility.checkInternetConnectivity()) {
                if (this.searchKeyword != null) {
                    this.getSearchProgressBar().setVisibility(View.VISIBLE);
                    this.getSearchController().getSearchItems(
                            new ClientDB(this.appCompatActivity).getUserEmail(),
                            this.searchKeyword,
                            Utilities.getInstance().getItemBatch(this.itemBatch),
                            fetchChoice
                    );
                }
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void setPreLoadedSearchItems(JSONArray departmentItemsArray) {
        try {
            List<DepartmentItemDataObject> dataSet = Utilities.getInstance().parseItems(departmentItemsArray);
            this.isDataFetchNecessary = Utilities.getInstance().setIsDataFetchNecessary(dataSet.size());
            this.getDepartmentItems().getDepartmentItemDataObjects().addAll(dataSet);
            this.getDepartmentItems().getDepartmentItemAdapter().notifyItemRangeInserted(
                    this.getDepartmentItems().getDepartmentItemDataObjects().size() + 1,
                    dataSet.size()
            );
            Utilities.getInstance().hideSoftKeyBoard(
                    this.appCompatActivity,
                    this.appCompatActivity.getCurrentFocus()
            );
            this.getSearchProgressBar().setVisibility(View.GONE);
            this.itemBatch = Utilities.getInstance().incrementItemBatch(this.itemBatch);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void revealItemDetailLayout(DepartmentItemDataObject departmentItemDataObject, View itemView) {
        try {
            this.itemDetailLayoutManipulator.revealItemDetailLayout(departmentItemDataObject, itemView);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public DrawerLayout getSearchItemPageLayout() {
        return searchItemPageLayout;
    }

    public TextView getNoSearchResults() {
        return noSearchResults;
    }

    private void resetItemBatch() {
        this.itemBatch = 0;
    }

    public ItemDetailLayoutManipulator getItemDetailLayoutManipulator() {
        return itemDetailLayoutManipulator;
    }

    @Override
    public void startReconnecting() {
        try {
            Utilities.getInstance().hideSoftKeyBoard(this.appCompatActivity, this.searchItemPageLayout);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void tryToReconnect() {
        try {
            this.fetchSearchItems(EItemFetchChoice.FETCHING_FIRST_TIME);
            CartObserver.getCartObserverInstance().instantiateCart();
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void finishSearchActivity() {
        try {
            if (className.equals(DepartmentPage.class)) {
                Utilities.getInstance().startDepartmentActivity(this.appCompatActivity, departmentName, departmentId, categoryId);
            } else if (className.equals(CustomerHelpPage.class)) {
                Utilities.getInstance().startCustomerHelpActivity(this.appCompatActivity, pageName);
            } else {
                Utilities.getInstance().startNewActivityByFinishingOlder(this.appCompatActivity, className);
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }
}
