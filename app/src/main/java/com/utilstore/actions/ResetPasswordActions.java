package com.utilstore.actions;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.utilstore.analytics.LogEvents;
import com.utilstore.analytics.logging.Constants;
import com.utilstore.analytics.logging.ILog;
import com.utilstore.analytics.logging.Logger;
import com.utilstore.app.LoginPage;
import com.utilstore.app.R;
import com.utilstore.components.CustomComponents;
import com.utilstore.components.interfaces.IInternetConnectionObject;
import com.utilstore.components.listeners.LoginLinkListener;
import com.utilstore.components.listeners.ResetButtonClickListener;
import com.utilstore.datastore.ResetPasswordController;
import com.utilstore.utilities.InternetConnectionUtility;
import com.utilstore.utilities.Utilities;
import com.utilstore.utilities.Validators;

import org.json.JSONObject;

import java.util.HashMap;

import static com.utilstore.constants.Constants.EMAIL_PARAM;
import static com.utilstore.constants.Constants.MESSAGE;

/**
 * Created by anfal on 12/25/2015.
 */
public class ResetPasswordActions implements IInternetConnectionObject {

    private static ResetPasswordActions resetPasswordActions = null;
    private AppCompatActivity appCompatActivity = null;
    private ResetPasswordController resetPasswordController = null;
    private ILog logger = null;
    private EditText email = null;
    private ProgressDialog progressDialog = null;
    private InternetConnectionUtility internetConnectionUtility = null;

    private ResetPasswordActions(AppCompatActivity appCompatActivity) {
        this.logger = Logger.getLogger();
        try {
            this.appCompatActivity = appCompatActivity;
            this.resetPasswordController = new ResetPasswordController(this.appCompatActivity);
            this.email = (EditText) this.appCompatActivity.findViewById(R.id.reset_email);
            TextView resetLoginLink = (TextView) this.appCompatActivity.findViewById(R.id.reset_login_text_link);
            Button resetButton = (Button) this.appCompatActivity.findViewById(R.id.reset_button);
            Utilities.getInstance().decorateHyperLink(resetLoginLink);
            resetButton.setOnClickListener(new ResetButtonClickListener());
            resetLoginLink.setOnClickListener(new LoginLinkListener());
            this.internetConnectionUtility = new InternetConnectionUtility(this.appCompatActivity);
            this.internetConnectionUtility.registerAsObserver(this);
        } catch (Exception ex) {
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public static ResetPasswordActions getResetPasswordActionsInstance() {
        return resetPasswordActions;
    }

    public static void setResetPasswordActionsInstance(AppCompatActivity appCompatActivity) {
        if (null == resetPasswordActions) {
            resetPasswordActions = new ResetPasswordActions(appCompatActivity);
        }
    }

    public void destroyResetPasswordActionsInstance() {
        if (null != resetPasswordActions) {
            resetPasswordActions = null;
        }
    }

    public void resetPassword() {
        try {
            if (this.internetConnectionUtility.checkInternetConnectivity()) {
                FlurryAgent.logEvent(
                        LogEvents.EVENT_ID_PASSWORD_RESET,
                        LogEvents.getEventData(
                                LogEvents.EVENT_ID_PASSWORD_RESET,
                                new String[]{
                                        LogEvents.TRIGGERED_RESET_BUTTON_CLICK_EVENT
                                }
                        )
                );
                String email = this.email.getText().toString();
                if (email.isEmpty()) {
                    CustomComponents.getInstance().messageDialog(this.appCompatActivity, this.appCompatActivity.getString(R.string.text_error_prompt), this.appCompatActivity.getString(R.string.alert_incomplete_information)).show();
                } else if (!Validators.getInstance().isEmailValid(email)) {
                    CustomComponents.getInstance().messageDialog(this.appCompatActivity, this.appCompatActivity.getString(R.string.text_error_prompt), this.appCompatActivity.getString(R.string.email_not_valid)).show();
                } else {
                    HashMap<String, String> resetPasswordParams = new HashMap<>();
                    resetPasswordParams.put(EMAIL_PARAM, email);
                    this.progressDialog = ProgressDialog.show(this.appCompatActivity, this.appCompatActivity.getString(R.string.text_resetting_password_prompt), this.appCompatActivity.getString(R.string.text_please_wait), true);
                    this.resetPasswordController.sendResetPasswordRequest(resetPasswordParams);
                }
            }
        } catch (Exception ex) {
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void displayUserMessage(JSONObject response) {
        try {
            this.progressDialog.dismiss();
            CustomComponents.getInstance().messageDialog(this.appCompatActivity, this.appCompatActivity.getString(R.string.text_success_prompt), response.getString(MESSAGE)).show();
        } catch (Exception ex) {
            this.progressDialog.dismiss();
            CustomComponents.getInstance().messageDialog(this.appCompatActivity, this.appCompatActivity.getString(R.string.text_error_prompt), this.appCompatActivity.getString(R.string.internal_error_occured)).show();
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void startLoginActivity() {
        try {
            Utilities.getInstance().startNewActivityByFinishingOlder(this.appCompatActivity, LoginPage.class);
        } catch (Exception ex) {
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void startReconnecting() {
        this.internetConnectionUtility.showNoInternetMessage();
    }

    @Override
    public void tryToReconnect() {
        this.resetPassword();
    }
}
