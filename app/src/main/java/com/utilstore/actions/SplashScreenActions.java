package com.utilstore.actions;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.utilstore.analytics.logging.Constants;
import com.utilstore.analytics.logging.ILog;
import com.utilstore.analytics.logging.Logger;
import com.utilstore.components.dataobjects.DeliveryChargesInfoDataObject;
import com.utilstore.components.interfaces.IInternetConnectionObject;
import com.utilstore.database.ClientDB;
import com.utilstore.datastore.SplashScreenController;
import com.utilstore.utilities.InternetConnectionUtility;
import com.utilstore.utilities.Utilities;

import org.json.JSONObject;

import static com.utilstore.constants.Constants.APP_VERSION_FLAG;
import static com.utilstore.constants.Constants.CHARGEDPRICE;
import static com.utilstore.constants.Constants.DEFAULT_EMAIL_ID;
import static com.utilstore.constants.Constants.DEFAULT_USER_ROLE;
import static com.utilstore.constants.Constants.MESSAGE;
import static com.utilstore.constants.Constants.STATUS;
import static com.utilstore.constants.Constants.SUCCESS;
import static com.utilstore.constants.Constants.THRESHOLDPRICE;

/**
 * Created by anfal on 1/14/2016.
 */

public class SplashScreenActions implements IInternetConnectionObject {

    private AppCompatActivity appCompatActivity = null;
    private ILog logger = null;
    private SplashScreenController splashScreenController = null;
    private ClientDB clientDB = null;
    private InternetConnectionUtility internetConnectionUtility = null;

    public SplashScreenActions(AppCompatActivity appCompatActivity) {
        this.logger = Logger.getLogger();
        this.appCompatActivity = appCompatActivity;
        this.internetConnectionUtility = new InternetConnectionUtility(this.appCompatActivity);
        internetConnectionUtility.registerAsObserver(this);
        this.splashScreenController = new SplashScreenController(this.appCompatActivity);
        this.clientDB = new ClientDB(this.appCompatActivity);
    }

    public void initiateUserInfoCheck() {
        try {
            if (this.internetConnectionUtility.checkInternetConnectivity()) {
                this.splashScreenController.getClientSyncInfo(this);
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void updateClientInfoIfNecessary(JSONObject response) {
        try {
            if (SUCCESS == Integer.parseInt(response.getString(STATUS))) {
                DeliveryChargesInfoDataObject deliveryChargesInfoDataObject = new ClientDB(this.appCompatActivity).getDeliveryChargesInfo();
                int thresholdPrice = Integer.parseInt(response.getString(THRESHOLDPRICE));
                int chargedPrice = Integer.parseInt(response.getString(CHARGEDPRICE));
                int versionFlag = Integer.parseInt(response.getString(APP_VERSION_FLAG));
                if (deliveryChargesInfoDataObject.getChargedPrice() != chargedPrice || deliveryChargesInfoDataObject.getThresholdPrice() != thresholdPrice) {
                    //This case is necessary because if user is coming first time to website, there will be novalue saved
                    //in db so in order to force delivery charges info this step is necessary.
                    if (!this.clientDB.isSessionNotEmpty()) {
                        this.clientDB.insertSessionRow(
                                DEFAULT_USER_ROLE,
                                DEFAULT_EMAIL_ID,
                                thresholdPrice,
                                chargedPrice
                        );
                    } else {
                        deliveryChargesInfoDataObject = new DeliveryChargesInfoDataObject();
                        deliveryChargesInfoDataObject.setChargedPrice(chargedPrice);
                        deliveryChargesInfoDataObject.setThresholdPrice(thresholdPrice);
                        this.clientDB.updateDeliveryChargesInSession(deliveryChargesInfoDataObject);
                    }
                }
                Utilities.getInstance().startStoreActivityFromSplashScreen(this.appCompatActivity, versionFlag);
            } else {
                Toast.makeText(this.appCompatActivity, response.getString(MESSAGE), Toast.LENGTH_LONG).show();
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void startReconnecting() {
        this.internetConnectionUtility.showNoInternetMessage();
    }

    @Override
    public void tryToReconnect() {

    }
}
