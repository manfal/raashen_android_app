package com.utilstore.actions;

import android.content.Context;

import com.utilstore.analytics.logging.ILog;
import com.utilstore.analytics.logging.Logger;
import com.utilstore.components.interfaces.IPhoneNumberVerificationInterface;
import com.utilstore.datastore.PhoneNumberVerficationCodeController;

import java.util.HashMap;

import static com.utilstore.constants.Constants.EMAIL_PARAM;
import static com.utilstore.constants.Constants.VERIFICATION_CODE_PARAM;

/**
 * Created by anfal on 1/29/2016.
 */
public class PhoneNumberVerificationAction {

    private ILog logger = null;

    public PhoneNumberVerificationAction() {
        this.logger = Logger.getLogger();
    }

    public void sendVerficationCodeConfirmationRequest(Context context, IPhoneNumberVerificationInterface phoneNumberVerificationInterface, String verificationCode, String email) {
        HashMap<String, String> phoneVerificationParams = new HashMap<>();
        phoneVerificationParams.put(EMAIL_PARAM, email);
        phoneVerificationParams.put(VERIFICATION_CODE_PARAM, verificationCode);
        PhoneNumberVerficationCodeController phoneNumberVerficationCodeController = new PhoneNumberVerficationCodeController(context);
        phoneNumberVerficationCodeController.makePhoneNumberVerificationRequest(phoneVerificationParams, phoneNumberVerificationInterface);
    }
}
