package com.utilstore.actions;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.utilstore.analytics.logging.Constants;
import com.utilstore.analytics.logging.ILog;
import com.utilstore.analytics.logging.Logger;
import com.utilstore.app.R;
import com.utilstore.components.dataobjects.DepartmentItemDataObject;
import com.utilstore.components.listeners.DepartmentItemsRecyclerViewScrollListener;
import com.utilstore.components.navigation.DepartmentItems;
import com.utilstore.database.ClientDB;
import com.utilstore.datastore.DepartmentItemsFragmentController;
import com.utilstore.enumerations.EItemFetchChoice;
import com.utilstore.utilities.Utilities;

import org.json.JSONArray;

import java.util.List;

/**
 * Created by anfal on 12/28/2015.
 */
public class DepartmentCategoryFragmentActions {

    private View view = null;
    private ILog logger = null;
    private ProgressBar progressBar = null;
    private DepartmentItemsFragmentController departmentItemsFragmentController = null;
    private DepartmentItems departmentItems = null;
    private int departmentId, categoryId, itemBatch = 0;
    private boolean isDataFetchNecessary;

    public DepartmentCategoryFragmentActions(View view, int departmentId, int categoryId) {
        this.view = view;
        this.logger = Logger.getLogger();
        RecyclerView departmentCategoryItemsRecyclerView = (RecyclerView) this.view.findViewById(R.id.department_items);
        departmentCategoryItemsRecyclerView.addOnScrollListener(new DepartmentItemsRecyclerViewScrollListener(this));
        this.progressBar = (ProgressBar) this.view.findViewById(R.id.department_items_progress);
        this.setDepartmentId(departmentId);
        this.setCategoryId(categoryId);
        this.isDataFetchNecessary = true;
        this.departmentItemsFragmentController = new DepartmentItemsFragmentController(this.view.getContext());
        this.departmentItems = new DepartmentItems(departmentCategoryItemsRecyclerView, this.view.getContext());
    }

    public ProgressBar getProgressBar() {
        return this.progressBar;
    }

    public void setPreLoadedDepartmentItems(JSONArray departmentItemsArray) {
        try {
            List<DepartmentItemDataObject> dataSet = Utilities.getInstance().parseItems(departmentItemsArray);
            this.isDataFetchNecessary = Utilities.getInstance().setIsDataFetchNecessary(dataSet.size());
            this.departmentItems.getDepartmentItemDataObjects().addAll(dataSet);
            this.departmentItems.getDepartmentItemAdapter().notifyItemRangeInserted(
                    this.departmentItems.getDepartmentItemDataObjects().size() + 1,
                    dataSet.size()
            );
            this.getProgressBar().setVisibility(View.GONE);
            this.itemBatch = Utilities.getInstance().incrementItemBatch(this.itemBatch);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void setDepartmentItems(JSONArray departmentItemsArray) {
        try {
            List<DepartmentItemDataObject> dataSet = Utilities.getInstance().parseItems(departmentItemsArray);
            this.isDataFetchNecessary = Utilities.getInstance().setIsDataFetchNecessary(dataSet.size());
            boolean areItemsLoaded = this.departmentItems.instantiateDepartmentItems(dataSet);
            if (areItemsLoaded) {
                this.getProgressBar().setVisibility(View.GONE);
                this.itemBatch = Utilities.getInstance().incrementItemBatch(this.itemBatch);
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

    public void fetchItems(EItemFetchChoice fetchChoice) {
        try {
            this.getProgressBar().setVisibility(View.VISIBLE);
            this.departmentItemsFragmentController.getDepartmentItems(
                    new ClientDB(this.view.getContext()).getUserEmail(),
                    this.getDepartmentId(),
                    this.getCategoryId(),
                    Utilities.getInstance().getItemBatch(this.itemBatch),
                    fetchChoice,
                    this
            );
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public boolean isDataFetchNecessary() {
        return isDataFetchNecessary;
    }
}