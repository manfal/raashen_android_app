package com.utilstore.constants;

/**
 * Created by anfal on 12/8/2015.
 */
public class Config {
    //public static String API_HOST = "http://192.168.43.121/braashen/";
    public static final String UTILSTORE_API_VERSION = "api/v1/";
    //public static String API_HOST = "http://192.168.8.100/raashen/";
    //public static String API_HOST = "http://192.168.8.101/raashen/";
    //public static String API_HOST = "http://10.0.2.2/braashen/";
    public static String API_HOST = "http://www.raashen.pk/";

    private Config() {
    }
}