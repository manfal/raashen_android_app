package com.utilstore.datastore;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.utilstore.actions.CheckoutPageActions;
import com.utilstore.analytics.logging.Constants;
import com.utilstore.analytics.logging.ILog;
import com.utilstore.analytics.logging.Logger;
import com.utilstore.utilities.VolleyInstance;

import org.json.JSONObject;

import java.util.HashMap;

import static com.utilstore.constants.Constants.INVOICE_REVIEW_REQUEST_PARAMS;
import static com.utilstore.constants.Constants.REQUEST_RETRY_TIMEOUT;
import static com.utilstore.constants.Constants.REQUST_RETRY_COUNT;
import static com.utilstore.constants.Constants.USER_GET_ACCOUNT_INFO_PARAM;
import static com.utilstore.constants.HttpRoutes.ADD_PHONE_NUMBER_ROUTE;
import static com.utilstore.constants.HttpRoutes.GET_INVOICE_REVIEW_INFO_ROUTE;
import static com.utilstore.constants.HttpRoutes.GET_PRE_CHECKOUT_DATA_ROUTE;
import static com.utilstore.constants.HttpRoutes.STORE_DELIVERY_ITEMS_REQUST;

/**
 * Created by anfal on 1/28/2016.
 */
public class CheckoutPageController {
    private ILog logger = null;
    private Context context = null;

    public CheckoutPageController(Context context) {
        this.logger = Logger.getLogger();
        try {
            this.context = context;
        } catch (Exception ex) {
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void getPreCheckoutData(String email) {
        try {
            String getPreCheckoutData = String.format(
                    GET_PRE_CHECKOUT_DATA_ROUTE + USER_GET_ACCOUNT_INFO_PARAM,
                    email
            );
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, getPreCheckoutData, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    CheckoutPageActions.getCheckoutPageActionsInstance().populatePreCheckoutData(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(error), error.getCause());
                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(REQUEST_RETRY_TIMEOUT, REQUST_RETRY_COUNT, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyInstance.getInstance(this.context).addToRequestQueue(jsonObjectRequest);

        } catch (Exception ex) {
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void sendPhoneNumberRequest(HashMap<String, String> phoneNumberData) {
        try {
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, ADD_PHONE_NUMBER_ROUTE, new JSONObject(phoneNumberData), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    CheckoutPageActions.getCheckoutPageActionsInstance().postPhoneNumberAdditionTasks(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(error), error.getCause());
                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(REQUEST_RETRY_TIMEOUT, REQUST_RETRY_COUNT, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyInstance.getInstance(this.context).addToRequestQueue(jsonObjectRequest);

        } catch (Exception ex) {
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void fetchInvoiceReviewData(String email, String addressLabel) {
        try {
            String invoiceReviewRequestString = String.format(
                    GET_INVOICE_REVIEW_INFO_ROUTE + INVOICE_REVIEW_REQUEST_PARAMS,
                    email,
                    addressLabel
            );

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, invoiceReviewRequestString, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    CheckoutPageActions.getCheckoutPageActionsInstance().loadInvoiceReviewData(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(error), error.getCause());
                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(REQUEST_RETRY_TIMEOUT, REQUST_RETRY_COUNT, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyInstance.getInstance(this.context).addToRequestQueue(jsonObjectRequest);

        } catch (Exception ex) {
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void sendCheckoutItems(HashMap<String, Object> checkoutItems) {
        try {
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, STORE_DELIVERY_ITEMS_REQUST, new JSONObject(checkoutItems), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    CheckoutPageActions.getCheckoutPageActionsInstance().cleanUpAfterCheckout(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(error), error.getCause());
                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(REQUEST_RETRY_TIMEOUT, REQUST_RETRY_COUNT, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyInstance.getInstance(this.context).addToRequestQueue(jsonObjectRequest);

        } catch (Exception ex) {
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }
}
