package com.utilstore.datastore;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.utilstore.analytics.logging.Constants;
import com.utilstore.analytics.logging.ILog;
import com.utilstore.analytics.logging.Logger;
import com.utilstore.components.interfaces.IPhoneNumberVerificationInterface;
import com.utilstore.utilities.VolleyInstance;

import org.json.JSONObject;

import java.util.HashMap;

import static com.utilstore.constants.Constants.REQUEST_RETRY_TIMEOUT;
import static com.utilstore.constants.Constants.REQUST_RETRY_COUNT;
import static com.utilstore.constants.HttpRoutes.PHONE_NUMBER_VERIFICATION_ROUTE;

/**
 * Created by anfal on 1/29/2016.
 */
public class PhoneNumberVerficationCodeController {
    private ILog logger = null;
    private Context context = null;

    public PhoneNumberVerficationCodeController(Context context) {
        this.logger = Logger.getLogger();
        try {
            this.context = context;
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void makePhoneNumberVerificationRequest(HashMap<String, String> phoneVerificationParams, final IPhoneNumberVerificationInterface phoneNumberVerificationInterface) {
        try {
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, PHONE_NUMBER_VERIFICATION_ROUTE, new JSONObject(phoneVerificationParams), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        phoneNumberVerificationInterface.postVerificationChores(response);
                    } catch (Exception ex) {
                        logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(error), error.getCause());
                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(REQUEST_RETRY_TIMEOUT, REQUST_RETRY_COUNT, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyInstance.getInstance(this.context).addToRequestQueue(jsonObjectRequest);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }
}
