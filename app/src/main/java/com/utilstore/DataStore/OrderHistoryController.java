package com.utilstore.datastore;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.utilstore.actions.OrderHistoryActions;
import com.utilstore.analytics.logging.Constants;
import com.utilstore.analytics.logging.ILog;
import com.utilstore.analytics.logging.Logger;
import com.utilstore.enumerations.EItemFetchChoice;
import com.utilstore.utilities.VolleyInstance;

import org.json.JSONObject;

import java.util.Map;

import static com.utilstore.constants.Constants.BATCH_SIZE_KEY;
import static com.utilstore.constants.Constants.BATCH_START_KEY;
import static com.utilstore.constants.Constants.INVOICE_DETAIL_PARAMS;
import static com.utilstore.constants.Constants.ORDER_HISTORY_PARAMS;
import static com.utilstore.constants.Constants.REQUEST_RETRY_TIMEOUT;
import static com.utilstore.constants.Constants.REQUST_RETRY_COUNT;
import static com.utilstore.constants.HttpRoutes.INVOICE_DETAIL_ROUTE;
import static com.utilstore.constants.HttpRoutes.ORDER_HISTORY_ROUTE;

/**
 * Created by anfal on 1/8/2016.
 */
public class OrderHistoryController {

    private ILog logger = null;
    private Context context = null;

    public OrderHistoryController(Context context) {
        this.logger = Logger.getLogger();
        try {
            this.context = context;
        } catch (Exception ex) {
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void getOrderHistory(String email, Map<String, Integer> batch, final EItemFetchChoice fetchChoice) {
        try {
            String orderHistoryRoute = String.format(
                    ORDER_HISTORY_ROUTE + ORDER_HISTORY_PARAMS,
                    email,
                    batch.get(BATCH_START_KEY),
                    batch.get(BATCH_SIZE_KEY)
            );

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, orderHistoryRoute, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    switch (fetchChoice) {
                        case FETCHING_FIRST_TIME:
                            OrderHistoryActions.getOrderHistoryActionsInstance().setOrderHistory(response);
                            break;
                        case FETCHING_TO_APPEND:
                            OrderHistoryActions.getOrderHistoryActionsInstance().appendOrderHistory(response);
                            break;
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(error), error.getCause());
                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(REQUEST_RETRY_TIMEOUT, REQUST_RETRY_COUNT, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyInstance.getInstance(this.context).addToRequestQueue(jsonObjectRequest);
        } catch (Exception ex) {
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void getInvoiceDetail(int invoiceId) {
        try {
            String invoiceDetailParams = String.format(
                    INVOICE_DETAIL_ROUTE + INVOICE_DETAIL_PARAMS,
                    invoiceId
            );
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, invoiceDetailParams, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    OrderHistoryActions.getOrderHistoryActionsInstance().populateInvoiceDetail(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(error), error.getCause());
                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(REQUEST_RETRY_TIMEOUT, REQUST_RETRY_COUNT, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyInstance.getInstance(this.context).addToRequestQueue(jsonObjectRequest);
        } catch (Exception ex) {
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }
}
