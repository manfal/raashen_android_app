package com.utilstore.datastore;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.utilstore.actions.SearchActions;
import com.utilstore.analytics.logging.Constants;
import com.utilstore.analytics.logging.ILog;
import com.utilstore.analytics.logging.Logger;
import com.utilstore.enumerations.EItemFetchChoice;
import com.utilstore.utilities.VolleyInstance;

import org.json.JSONObject;

import java.util.Map;

import static com.utilstore.constants.Constants.BATCH_SIZE_KEY;
import static com.utilstore.constants.Constants.BATCH_START_KEY;
import static com.utilstore.constants.Constants.ITEM_LIST;
import static com.utilstore.constants.Constants.REQUEST_RETRY_TIMEOUT;
import static com.utilstore.constants.Constants.REQUST_RETRY_COUNT;
import static com.utilstore.constants.Constants.SEARCH_ITEMS_PARAMS;
import static com.utilstore.constants.HttpRoutes.SEARCH_ITEMS_ROUTE;

/**
 * Created by anfal on 12/31/2015.
 */
public class SearchController {
    private ILog logger = null;
    private Context context = null;

    public SearchController(Context context) {
        this.logger = Logger.getLogger();
        try {
            this.context = context;
        } catch (Exception ex) {
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void getSearchItems(String email, String searchKeyword, Map<String, Integer> batch, final EItemFetchChoice fetchChoice) {
        try {
            String searchItemsRequestString = String.format(
                    SEARCH_ITEMS_ROUTE + SEARCH_ITEMS_PARAMS,
                    searchKeyword,
                    batch.get(BATCH_START_KEY),
                    batch.get(BATCH_SIZE_KEY),
                    email
            );

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, searchItemsRequestString, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        switch (fetchChoice) {
                            case FETCHING_FIRST_TIME:
                                SearchActions.getSearchActionsInstance().setSearchedItems(response.getJSONArray(ITEM_LIST));
                                break;
                            case FETCHING_TO_APPEND:
                                SearchActions.getSearchActionsInstance().setPreLoadedSearchItems(response.getJSONArray(ITEM_LIST));
                                break;
                        }
                    } catch (Exception ex) {
                        logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(error), error.getCause());
                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(REQUEST_RETRY_TIMEOUT, REQUST_RETRY_COUNT, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyInstance.getInstance(this.context).addToRequestQueue(jsonObjectRequest);
        } catch (Exception ex) {
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }
}
