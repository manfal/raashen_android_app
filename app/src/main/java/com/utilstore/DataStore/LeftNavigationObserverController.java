package com.utilstore.datastore;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.utilstore.analytics.logging.Constants;
import com.utilstore.analytics.logging.ILog;
import com.utilstore.analytics.logging.Logger;
import com.utilstore.components.observers.LeftNavigationObserver;
import com.utilstore.utilities.VolleyInstance;

import org.json.JSONObject;

import static com.utilstore.constants.Constants.NAVLIST;
import static com.utilstore.constants.Constants.REQUEST_RETRY_TIMEOUT;
import static com.utilstore.constants.Constants.REQUST_RETRY_COUNT;
import static com.utilstore.constants.HttpRoutes.NAV_ITEMS_ROUTE;

/**
 * Created by anfal on 12/19/2015.
 */
public class LeftNavigationObserverController {
    private ILog logger = null;
    private Context context = null;

    public LeftNavigationObserverController(Context context) {
        this.logger = Logger.getLogger();
        this.context = context;
    }

    public void getLeftMenu(final LeftNavigationObserver leftNavigationObserver) throws Exception {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, NAV_ITEMS_ROUTE, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    leftNavigationObserver.populateLeftMenu(response.getJSONArray(NAVLIST));
                } catch (Exception ex) {
                    logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(error), error.getCause());
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(REQUEST_RETRY_TIMEOUT, REQUST_RETRY_COUNT, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyInstance.getInstance(this.context).addToRequestQueue(jsonObjectRequest);
    }
}
