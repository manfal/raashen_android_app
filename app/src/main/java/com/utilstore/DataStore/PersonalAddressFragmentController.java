package com.utilstore.datastore;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.utilstore.actions.PersonalAddressFragmentActionsSender;
import com.utilstore.analytics.logging.Constants;
import com.utilstore.analytics.logging.ILog;
import com.utilstore.analytics.logging.Logger;
import com.utilstore.utilities.VolleyInstance;

import org.json.JSONObject;

import java.util.HashMap;

import static com.utilstore.constants.Constants.ADDRESSES;
import static com.utilstore.constants.Constants.REQUEST_RETRY_TIMEOUT;
import static com.utilstore.constants.Constants.REQUST_RETRY_COUNT;
import static com.utilstore.constants.Constants.USER_GET_ACCOUNT_INFO_PARAM;
import static com.utilstore.constants.HttpRoutes.DELETE_ADDRESS_ROUTE;
import static com.utilstore.constants.HttpRoutes.EDIT_ADDRESS_ROUTE;
import static com.utilstore.constants.HttpRoutes.USER_ACCOUNT_ADDRESSES_ROUTE;

/**
 * Created by anfal on 1/5/2016.
 */
public class PersonalAddressFragmentController {

    private ILog logger = null;
    private Context context = null;

    public PersonalAddressFragmentController(Context context) {
        this.logger = Logger.getLogger();
        try {
            this.context = context;
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void getExistingAddresses(String email) {
        try {
            String userAddressRequestString = String.format(
                    USER_ACCOUNT_ADDRESSES_ROUTE + USER_GET_ACCOUNT_INFO_PARAM,
                    email
            );

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, userAddressRequestString, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        PersonalAddressFragmentActionsSender.getPersonalAddressFragmentActions().setExistingAddresses(response.getJSONArray(ADDRESSES));
                    } catch (Exception ex) {
                        logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(error), error.getCause());
                }
            });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(REQUEST_RETRY_TIMEOUT, REQUST_RETRY_COUNT, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyInstance.getInstance(this.context).addToRequestQueue(jsonObjectRequest);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void makeDeleteAddressRequest(HashMap<String, Integer> deletAddressInfo) {
        try {
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, DELETE_ADDRESS_ROUTE, new JSONObject(deletAddressInfo), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        PersonalAddressFragmentActionsSender.getPersonalAddressFragmentActions().deletedDataCleanUp(response);
                    } catch (Exception ex) {
                        logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(error), error.getCause());
                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(REQUEST_RETRY_TIMEOUT, REQUST_RETRY_COUNT, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyInstance.getInstance(this.context).addToRequestQueue(jsonObjectRequest);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void makeEditAddressRequest(HashMap<String, Object> editAddressInfo) {
        try {
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, EDIT_ADDRESS_ROUTE, new JSONObject(editAddressInfo), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        PersonalAddressFragmentActionsSender.getPersonalAddressFragmentActions().postAddressEditChores(response);
                    } catch (Exception ex) {
                        logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(error), error.getCause());
                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(REQUEST_RETRY_TIMEOUT, REQUST_RETRY_COUNT, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyInstance.getInstance(this.context).addToRequestQueue(jsonObjectRequest);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }
}
