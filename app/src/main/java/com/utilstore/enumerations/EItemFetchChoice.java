package com.utilstore.enumerations;

/**
 * Created by anfal on 12/21/2015.
 */
public enum EItemFetchChoice {
    FETCHING_FIRST_TIME,
    FETCHING_TO_APPEND
}
