package com.utilstore.database.schema;

import com.utilstore.database.contracts.Contracts;

/**
 * Created by anfal on 12/9/2015.
 */
public class ClientDB {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "ClientDB.db";
    public static final String TEXT_TYPE = " TEXT";
    public static final String INT_TYPE = " INTEGER";
    public static final String NULL_TYPE = " NULL";
    public static final String COMMA_SEP = ",";

    public static final String SQL_CREATE_SESSION_ENTRIES = "CREATE TABLE " + Contracts.SessionsEntry.TABLE_NAME + " (" +
            Contracts.SessionsEntry._ID + " " + INT_TYPE + " PRIMARY KEY," +
            Contracts.SessionsEntry.COLUMN_NAME_ROLE + TEXT_TYPE + COMMA_SEP +
            Contracts.SessionsEntry.COLUMN_NAME_EMAIL + TEXT_TYPE + COMMA_SEP +
            Contracts.SessionsEntry.COLUMN_NAME_THRESHOLDPRICE + INT_TYPE + COMMA_SEP +
            Contracts.SessionsEntry.COLUMN_NAME_CHARGEDPRICE + INT_TYPE +
            " )";

    public static final String SQL_CREATE_ORDER_HISTORY_ENTRIES = "CREATE TABLE " + Contracts.OrderHistoryEntry.TABLE_NAME + " (" +
            Contracts.OrderHistoryEntry._ID + " " + INT_TYPE + " PRIMARY KEY," +
            Contracts.OrderHistoryEntry.COLUMN_NAME_ITEM_ID + INT_TYPE + COMMA_SEP +
            Contracts.OrderHistoryEntry.COLUMN_NAME_ITEM_NAME + TEXT_TYPE + COMMA_SEP +
            Contracts.OrderHistoryEntry.COLUMN_NAME_ITEM_WEIGHT_OR_QUANTITY + TEXT_TYPE + COMMA_SEP +
            Contracts.OrderHistoryEntry.COLUMN_NAME_ITEM_PRICE + INT_TYPE + COMMA_SEP +
            Contracts.OrderHistoryEntry.COLUMN_NAME_ITEM_DISCOUNT + INT_TYPE + COMMA_SEP +
            Contracts.OrderHistoryEntry.COLUMN_NAME_ITEM_CHECKOUT_QUANTITY + INT_TYPE +
            " )";


    public static final String SQL_DELETE_SESSION_ENTRIES = "DROP TABLE IF EXISTS " + Contracts.SessionsEntry.TABLE_NAME;

    public static final String SQL_DELETE_ORDER_HISTORY_ENTRIES = "DROP TABLE IF EXISTS " + Contracts.OrderHistoryEntry.TABLE_NAME;
}
