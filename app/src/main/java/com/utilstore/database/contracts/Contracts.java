package com.utilstore.database.contracts;

import android.provider.BaseColumns;

/**
 * Created by anfal on 12/9/2015.
 */
public class Contracts {
    public Contracts() {
    }

    public static abstract class SessionsEntry implements BaseColumns {
        public static final String TABLE_NAME = "us_sessions";
        public static final String COLUMN_NAME_ROLE = "us_role";
        public static final String COLUMN_NAME_EMAIL = "us_email";
        public static final String COLUMN_NAME_THRESHOLDPRICE = "us_threshold_price";
        public static final String COLUMN_NAME_CHARGEDPRICE = "us_charged_price";
        public static final String SORT_ORDER_DESC = " DESC";
        public static final String SORT_ORDER_ASC = " ASC";
    }

    public static abstract class OrderHistoryEntry implements BaseColumns {
        public static final String TABLE_NAME = "us_order_history";
        public static final String COLUMN_NAME_ITEM_ID = "oh_item_id";
        public static final String COLUMN_NAME_ITEM_NAME = "oh_item_name";
        public static final String COLUMN_NAME_ITEM_WEIGHT_OR_QUANTITY = "oh_item_weight_or_quantity";
        public static final String COLUMN_NAME_ITEM_PRICE = "oh_item_price";
        public static final String COLUMN_NAME_ITEM_DISCOUNT = "oh_item_discount";
        public static final String COLUMN_NAME_ITEM_CHECKOUT_QUANTITY = "oh_checkout_quantity";
        public static final String SORT_ORDER_DESC = " DESC";
        public static final String SORT_ORDER_ASC = " ASC";
    }
}
