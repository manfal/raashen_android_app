package com.utilstore.database.helpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.utilstore.database.schema.ClientDB.DATABASE_NAME;
import static com.utilstore.database.schema.ClientDB.DATABASE_VERSION;
import static com.utilstore.database.schema.ClientDB.SQL_CREATE_ORDER_HISTORY_ENTRIES;
import static com.utilstore.database.schema.ClientDB.SQL_CREATE_SESSION_ENTRIES;
import static com.utilstore.database.schema.ClientDB.SQL_DELETE_ORDER_HISTORY_ENTRIES;
import static com.utilstore.database.schema.ClientDB.SQL_DELETE_SESSION_ENTRIES;

/**
 * Created by anfal on 12/9/2015.
 */
public class ClientDBHelper extends SQLiteOpenHelper {

    public ClientDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_SESSION_ENTRIES);
        db.execSQL(SQL_CREATE_ORDER_HISTORY_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_SESSION_ENTRIES);
        db.execSQL(SQL_DELETE_ORDER_HISTORY_ENTRIES);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
