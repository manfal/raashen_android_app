package com.utilstore.components.proxies;

import android.view.View;

import com.utilstore.actions.DepartmentActions;
import com.utilstore.actions.SearchActions;
import com.utilstore.components.dataobjects.DepartmentItemDataObject;

/**
 * Created by anfal on 12/31/2015.
 */
public class DepartmentItemClickProxy {

    public void itemClicked(View v, DepartmentItemDataObject departmentItemDataObject) {
        if (null != SearchActions.getSearchActionsInstance()) {
            SearchActions.getSearchActionsInstance().revealItemDetailLayout(departmentItemDataObject, v);
        } else {
            DepartmentActions.getDepartmentActionInstance().revealItemDetailLayout(departmentItemDataObject, v);
        }
    }

}
