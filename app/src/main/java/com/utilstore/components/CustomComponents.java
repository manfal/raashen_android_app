package com.utilstore.components;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.utilstore.actions.CheckoutPageActions;
import com.utilstore.actions.PersonalAddressFragmentActionsSender;
import com.utilstore.actions.PersonalInfoFragmentActions;
import com.utilstore.app.R;
import com.utilstore.components.dataobjects.AddressDataObject;
import com.utilstore.components.interfaces.IAddressResponseSenderReceiver;
import com.utilstore.components.interfaces.IPhoneNumberVerificationInterface;
import com.utilstore.utilities.Utilities;

import static com.utilstore.constants.HttpRoutes.OPEN_BROWSER_LINK_FOR_APP;
import static com.utilstore.constants.HttpRoutes.OPEN_GOOGLE_PLAY_LINK_FOR_APP;


/**
 * Created by anfal on 12/8/2015.
 */
public class CustomComponents {
    private static CustomComponents ourInstance = new CustomComponents();

    private CustomComponents() {
    }

    public static CustomComponents getInstance() {
        return ourInstance;
    }

    public AlertDialog.Builder showNewVersionIsAvailableDialog(final Context context) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.attention_text);
        builder.setMessage(R.string.new_version_is_available_text);
        builder.setPositiveButton(R.string.update_text, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final String appPackageName = context.getPackageName();
                try {
                    Utilities.getInstance().startNewActivityFromIntent(context, new Intent(Intent.ACTION_VIEW, Uri.parse(OPEN_GOOGLE_PLAY_LINK_FOR_APP + appPackageName)));
                } catch (Exception ex) {
                    Utilities.getInstance().startNewActivityFromIntent(context, new Intent(Intent.ACTION_VIEW, Uri.parse(OPEN_BROWSER_LINK_FOR_APP + appPackageName)));
                }
            }
        });
        builder.setNegativeButton(R.string.not_now_text, null);
        builder.setCancelable(false);
        return builder;
    }

    public AlertDialog.Builder messageDialog(Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setNegativeButton(context.getString(R.string.dialog_ok_button_text), null);
        return builder;
    }

    public AlertDialog.Builder userPasswordEditDialog(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = View.inflate(context, R.layout.myaccount_edit_password_dialog, null);
        builder.setView(view);
        builder.setNegativeButton(context.getString(R.string.dialog_cancel_button_text), null);
        builder.setPositiveButton(context.getString(R.string.dialog_ok_button_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (null != PersonalInfoFragmentActions.getPersonalInfoActionInstance()) {
                    PersonalInfoFragmentActions.getPersonalInfoActionInstance().handlePasswordEdit();
                }
            }
        });
        return builder;
    }

    public AlertDialog.Builder userPersonalInfoEditDialog(Context context, LinearLayout personalInfoEditLayout, String firstName, String lastName, String phNum) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View personalInfoDialogView = LayoutInflater.from(context).inflate(R.layout.myaccount_edit_personal_info_dialog, personalInfoEditLayout);
        final EditText fName = (EditText) personalInfoDialogView.findViewById(R.id.first_name);
        final EditText lName = (EditText) personalInfoDialogView.findViewById(R.id.last_name);
        final EditText phoneNumber = (EditText) personalInfoDialogView.findViewById(R.id.phone_number);
        fName.setText(firstName);
        lName.setText(lastName);
        phoneNumber.setText(phNum);
        builder.setView(personalInfoDialogView);
        builder.setNegativeButton(context.getString(R.string.dialog_cancel_button_text), null);
        builder.setPositiveButton(context.getString(R.string.dialog_ok_button_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (null != PersonalInfoFragmentActions.getPersonalInfoActionInstance()) {
                    PersonalInfoFragmentActions.getPersonalInfoActionInstance().handlePersonalInfoEdit(
                            fName.getText().toString(),
                            lName.getText().toString(),
                            phoneNumber.getText().toString()
                    );
                }
            }
        });
        return builder;
    }

    public void verifyPhoneNumberDialog(Context context, final IPhoneNumberVerificationInterface phoneNumberVerificationInterface) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = View.inflate(context, R.layout.myaccount_phone_verification_dialog, null);
        final EditText verificationCode = (EditText) view.findViewById(R.id.verification_code);
        builder.setView(view);
        builder.setNegativeButton(context.getString(R.string.dialog_cancel_button_text), null);
        builder.setPositiveButton(context.getString(R.string.dialog_ok_button_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                phoneNumberVerificationInterface.handlePhoneNumberVerification(verificationCode.getText().toString());
            }
        });
        builder.show();
    }

    public void newAddressDialog(Context context, final IAddressResponseSenderReceiver addressResponseSenderReceiver) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final View dialogView = View.inflate(context, R.layout.myaccount_address_dialog, null);
        final EditText label = (EditText) dialogView.findViewById(R.id.addr_label);
        final EditText address = (EditText) dialogView.findViewById(R.id.address_text);
        final EditText city = (EditText) dialogView.findViewById(R.id.address_city);
        builder.setView(dialogView);
        builder.setNegativeButton(context.getString(R.string.dialog_cancel_button_text), null);
        builder.setPositiveButton(context.getString(R.string.dialog_ok_button_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                addressResponseSenderReceiver.sendNewAddressRequest(label.getText().toString(), address.getText().toString(), city.getText().toString());
            }
        });
        builder.show();
    }

    public AlertDialog.Builder deleteAddressConfirmActionDialog(Context context, String message, final int addressId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message);
        builder.setNegativeButton(context.getString(R.string.dialog_cancel_button_text), null);
        builder.setPositiveButton(context.getString(R.string.dialog_ok_button_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (null != PersonalInfoFragmentActions.getPersonalInfoActionInstance()) {
                    PersonalAddressFragmentActionsSender.getPersonalAddressFragmentActions().startAddressDeletion(addressId);
                }
            }
        });
        return builder;
    }

    public AlertDialog.Builder editAddressDialog(Context context, ViewGroup viewGroup, final AddressDataObject addressDataObject) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View editDialogLayout = LayoutInflater.from(context).inflate(R.layout.myaccount_address_dialog, viewGroup);
        final EditText label = (EditText) editDialogLayout.findViewById(R.id.addr_label);
        final EditText address = (EditText) editDialogLayout.findViewById(R.id.address_text);
        final EditText city = (EditText) editDialogLayout.findViewById(R.id.address_city);
        label.setText(addressDataObject.getLabel());
        address.setText(addressDataObject.getAddress());
        city.setText(addressDataObject.getCity());
        builder.setView(editDialogLayout);
        builder.setNegativeButton(context.getString(R.string.dialog_cancel_button_text), null);
        builder.setPositiveButton(context.getString(R.string.dialog_ok_button_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (null != PersonalInfoFragmentActions.getPersonalInfoActionInstance()) {
                    PersonalAddressFragmentActionsSender.getPersonalAddressFragmentActions().startAddressEdit(
                            label.getText().toString(),
                            address.getText().toString(),
                            city.getText().toString(),
                            addressDataObject.getId()
                    );
                }
            }
        });
        return builder;
    }

    public void enterPhoneNumberDialog(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = View.inflate(context, R.layout.myaccount_phone_verification_dialog, null);
        view.bringToFront();
        TextView textView = (TextView) view.findViewById(R.id.phone_verification_dialog_text);
        final EditText editText = (EditText) view.findViewById(R.id.verification_code);
        editText.setInputType(InputType.TYPE_CLASS_PHONE);
        TextInputLayout textInputLayout = (TextInputLayout) view.findViewById(R.id.verification_code_inputlayout);
        textInputLayout.setHint(context.getString(R.string.phone_number_text));
        textView.setText(context.getString(R.string.enter_phone_number_text));
        builder.setView(view);
        builder.setNegativeButton(context.getString(R.string.dialog_cancel_button_text), null);
        builder.setPositiveButton(context.getString(R.string.dialog_ok_button_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (null != CheckoutPageActions.getCheckoutPageActionsInstance()) {
                    CheckoutPageActions.getCheckoutPageActionsInstance().sendNewPhoneNumberRequest(editText.getText().toString());
                }
            }
        });
        builder.show();
    }
}
