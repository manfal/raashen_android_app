package com.utilstore.components.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.utilstore.actions.PersonalAddressFragmentActionsSender;
import com.utilstore.analytics.logging.ILog;
import com.utilstore.analytics.logging.Logger;
import com.utilstore.app.R;
import com.utilstore.components.listeners.NewAddressListener;

import static com.utilstore.analytics.logging.Constants.ERROR;
import static com.utilstore.analytics.logging.Constants.TAG_ERROR;

/**
 * Created by anfal on 1/2/2016.
 */
public class MyAccountAddressesFragment extends Fragment {

    private ILog logger = null;

    public MyAccountAddressesFragment() {
        this.logger = Logger.getLogger();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.myaccount_addresses_fragment, container, false);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        try {
            ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.pesonal_address_progress);
            RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.existing_addresses);
            CardView newAddressCard = (CardView) view.findViewById(R.id.add_new_address_card);
            FrameLayout addressLayout = (FrameLayout) view.findViewById(R.id.address_layout);
            LinearLayout addressDialogLayout = (LinearLayout) view.findViewById(R.id.address_dialog_layout);

            PersonalAddressFragmentActionsSender.setPersonalAddressFragmentActions(
                    view.getContext(),
                    progressBar,
                    recyclerView,
                    newAddressCard,
                    addressLayout,
                    addressDialogLayout
            );

            newAddressCard.setOnClickListener(new NewAddressListener(PersonalAddressFragmentActionsSender.getPersonalAddressFragmentActions()));

            PersonalAddressFragmentActionsSender.getPersonalAddressFragmentActions().getExistingAddresses();
        } catch (Exception ex) {
            this.logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        PersonalAddressFragmentActionsSender.getPersonalAddressFragmentActions().destroyPersonalAddressFragmentActions();
    }
}
