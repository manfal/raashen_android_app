package com.utilstore.components.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.utilstore.components.observers.LeftNavigationObserver;

public class ChildViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private int position;

    public ChildViewHolder(View itemView) {
        super(itemView);
    }

    public void setMainItemClickToGetId(int position) {
        itemView.setOnClickListener(this);
        this.position = position;
    }

    @Override
    public void onClick(View v) {
        LeftNavigationObserver.getLeftNavigationObserverInstance().navigationItemClickListener(position);
    }
}