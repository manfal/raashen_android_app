package com.utilstore.components.viewholder;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.utilstore.app.R;
import com.utilstore.components.dataobjects.AddressDataObject;
import com.utilstore.components.listeners.AddressDeleteListener;
import com.utilstore.components.listeners.AddressEditListener;

/**
 * Created by anfal on 1/5/2016.
 */
public class AddressHolder extends RecyclerView.ViewHolder {

    private CardView cardView = null;
    private TextView label = null;
    private ImageView edit, delete = null;
    private AddressDataObject addressDataObject = null;

    public AddressHolder(View itemView) {
        super(itemView);
        this.cardView = (CardView) itemView.findViewById(R.id.address_card);
        this.label = (TextView) itemView.findViewById(R.id.myaccount_address_text);
        this.edit = (ImageView) itemView.findViewById(R.id.myaccount_address_edit);
        this.delete = (ImageView) itemView.findViewById(R.id.myaccount_address_delete);
    }

    public TextView getLabel() {
        return label;
    }

    public void addEditDeleteListeners() {
        this.edit.setOnClickListener(new AddressEditListener(this));
        this.delete.setOnClickListener(new AddressDeleteListener(this.addressDataObject));
    }

    public AddressDataObject getAddressDataObject() {
        return this.addressDataObject;
    }

    public void setAddressDataObject(AddressDataObject addressDataObject) {
        this.addressDataObject = addressDataObject;
    }
}
