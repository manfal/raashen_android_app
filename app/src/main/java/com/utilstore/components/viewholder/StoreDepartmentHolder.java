package com.utilstore.components.viewholder;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.utilstore.actions.StoreActions;
import com.utilstore.app.R;

/**
 * Created by anfal on 12/17/2015.
 */
public class StoreDepartmentHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private CardView cardView = null;
    private ImageView imageView = null;
    private TextView textView = null;
    private int departmentPosition = 0;

    public StoreDepartmentHolder(View itemView) {
        super(itemView);
        this.cardView = (CardView) itemView.findViewById(R.id.department_card);
        this.imageView = (ImageView) itemView.findViewById(R.id.department_image);
        this.textView = (TextView) itemView.findViewById(R.id.department_name);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        StoreActions.getStoreActionInstance().storeDepartmentClickListener(this.departmentPosition);
    }

    public ImageView getImageView() {
        return this.imageView;
    }

    public TextView getTextView() {
        return this.textView;
    }

    public void setDepartmentPosition(int departmentPosition) {
        this.departmentPosition = departmentPosition;
    }
}
