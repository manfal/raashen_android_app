package com.utilstore.components.viewholder;

import android.os.Build;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.TextView;

import com.utilstore.app.R;
import com.utilstore.utilities.Utilities;

/**
 * Created by anfal on 12/5/2015.
 */
public class ParentHolder extends ParentViewHolder {
    private static final float INITIAL_POSITION = 0.0f;
    private static final float ROTATED_POSITION = 180f;
    private static final float PIVOT_VALUE = 0.5f;
    private static final long DEFAULT_ROTATE_DURATION_MS = 200;
    private static final boolean HONEYCOMB_AND_ABOVE = Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
    public TextView titleTextView;
    public View view = null;
    private String firstItemOFMenuArray = null;

    public ParentHolder(View itemView) {
        super(itemView);
        titleTextView = (TextView) itemView.findViewById(R.id.titleParent);
        view = itemView.findViewById(R.id.nav_drawer_item_divider);
        this.firstItemOFMenuArray = Utilities.getInstance().getDefaultMenuArray(itemView.getContext())[0];
    }

    public void bind(String parentText) {
        titleTextView.setText(parentText);
        if (this.firstItemOFMenuArray.toLowerCase().equals(parentText.toLowerCase())) {
            view.setVisibility(View.VISIBLE);
        } else {
            view.setVisibility(View.GONE);
        }
    }

    @Override
    public void setExpanded(boolean expanded) {
        super.setExpanded(expanded);
        if (!HONEYCOMB_AND_ABOVE) {
            return;
        }

        if (expanded) {
            //mArrowExpandImageView.setRotation(ROTATED_POSITION);
        } else {
            //mArrowExpandImageView.setRotation(INITIAL_POSITION);
        }
    }

    @Override
    public void onExpansionToggled(boolean expanded) {
        super.onExpansionToggled(expanded);
        if (!HONEYCOMB_AND_ABOVE) {
            return;
        }

        RotateAnimation rotateAnimation = new RotateAnimation(ROTATED_POSITION,
                INITIAL_POSITION,
                RotateAnimation.RELATIVE_TO_SELF, PIVOT_VALUE,
                RotateAnimation.RELATIVE_TO_SELF, PIVOT_VALUE);
        rotateAnimation.setDuration(DEFAULT_ROTATE_DURATION_MS);
        rotateAnimation.setFillAfter(true);
        //mArrowExpandImageView.startAnimation(rotateAnimation);
    }
}
