package com.utilstore.components.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.utilstore.app.R;
import com.utilstore.components.dataobjects.ChildDataObject;
import com.utilstore.components.dataobjects.ParentDataObjectListener;
import com.utilstore.components.listeners.ParentListItemListener;
import com.utilstore.components.viewholder.ChildHolder;
import com.utilstore.components.viewholder.ParentHolder;

import java.util.List;

/**
 * Created by anfal on 12/5/2015.
 */
public class DrawerAdapter extends ExpandableRecyclerAdapter<ParentHolder, ChildHolder> {
    private LayoutInflater mInflater;

    public DrawerAdapter(Context context, List<? extends ParentListItemListener> parentItemList) {
        super(parentItemList);
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public ParentHolder onCreateParentViewHolder(ViewGroup parent) {
        View view = mInflater.inflate(R.layout.nav_drawer_item_parent, parent, false);
        return new ParentHolder(view);
    }

    @Override
    public ChildHolder onCreateChildViewHolder(ViewGroup parent) {
        View view = mInflater.inflate(R.layout.nav_drawer_item_child, parent, false);
        return new ChildHolder(view);
    }

    @Override
    public void onBindParentViewHolder(ParentHolder parentViewHolder, int position, ParentListItemListener parentListItemListener) {
        ParentDataObjectListener verticalParent = (ParentDataObjectListener) parentListItemListener;
        parentViewHolder.bind(verticalParent.getParentText());
    }

    @Override
    public void onBindChildViewHolder(ChildHolder childViewHolder, int position, Object childListItem) {
        ChildDataObject verticalChild = (ChildDataObject) childListItem;
        childViewHolder.bind(verticalChild.getChildText());
    }
}
