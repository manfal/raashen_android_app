package com.utilstore.components.adapter;

import com.utilstore.components.listeners.ParentListItemListener;
import com.utilstore.components.wrappers.ParentWrapper;

import java.util.ArrayList;
import java.util.List;

public class ExpandableRecyclerAdapterHelper {

    public static List<Object> generateParentChildItemList(List<? extends ParentListItemListener> parentItemList) {
        List<Object> parentWrapperList = new ArrayList<>();
        ParentListItemListener parentListItemListener;
        ParentWrapper parentWrapper;

        int parentListItemCount = parentItemList.size();
        for (int i = 0; i < parentListItemCount; i++) {
            parentListItemListener = parentItemList.get(i);
            parentWrapper = new ParentWrapper(parentListItemListener);
            parentWrapperList.add(parentWrapper);

            if (parentWrapper.isInitiallyExpanded()) {
                parentWrapper.setExpanded(true);

                int childListItemCount = parentWrapper.getChildItemList().size();
                for (int j = 0; j < childListItemCount; j++) {
                    parentWrapperList.add(parentWrapper.getChildItemList().get(j));
                }
            }
        }

        return parentWrapperList;
    }
}
