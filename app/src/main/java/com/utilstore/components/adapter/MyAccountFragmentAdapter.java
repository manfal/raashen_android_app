package com.utilstore.components.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anfal on 1/2/2016.
 */
public class MyAccountFragmentAdapter extends FragmentPagerAdapter {

    private List<Fragment> myAccountFragments = null;
    private List<String> fragmentTitle = null;

    public MyAccountFragmentAdapter(FragmentManager fm) {
        super(fm);
        this.myAccountFragments = new ArrayList<>();
        this.fragmentTitle = new ArrayList<>();
    }

    public void addFragment(Fragment fragment, String title) {
        this.myAccountFragments.add(fragment);
        this.fragmentTitle.add(title);
    }

    @Override
    public Fragment getItem(int position) {
        return this.myAccountFragments.get(position);
    }

    @Override
    public int getCount() {
        return this.myAccountFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return this.fragmentTitle.get(position);
    }
}
