package com.utilstore.components.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anfal on 12/27/2015.
 */
public class DepartmentCategoryAdapter extends FragmentStatePagerAdapter {

    private List<Fragment> departmentCategoryFragment = null;
    private List<String> categoryTitle = null;

    public DepartmentCategoryAdapter(FragmentManager fm) {
        super(fm);
        this.departmentCategoryFragment = new ArrayList<>();
        this.categoryTitle = new ArrayList<>();
    }

    public void addFragment(Fragment departmentCategoryFragment, String categoryTitle) {
        this.departmentCategoryFragment.add(departmentCategoryFragment);
        this.categoryTitle.add(categoryTitle);
    }

    @Override
    public Fragment getItem(int position) {
        return this.departmentCategoryFragment.get(position);
    }

    @Override
    public int getCount() {
        return this.departmentCategoryFragment.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return this.categoryTitle.get(position);
    }

    public List<String> getCategoryTitles() {
        return this.categoryTitle;
    }
}
