package com.utilstore.components.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.utilstore.app.R;
import com.utilstore.components.dataobjects.CartItemDataObject;
import com.utilstore.components.viewholder.CheckoutItemHolder;
import com.utilstore.utilities.ItemCheckoutUtility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anfal on 1/13/2016.
 */
public class CartItemAdapter extends RecyclerView.Adapter<CheckoutItemHolder> {

    private List<CartItemDataObject> cartItemDataObjects = null;
    private List<CheckoutItemHolder> checkoutItemHolders = null;

    public CartItemAdapter(List<CartItemDataObject> cartItemDataObjects) {
        this.cartItemDataObjects = cartItemDataObjects;
        this.checkoutItemHolders = new ArrayList<>();
    }

    @Override
    public CheckoutItemHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cart_list_item, viewGroup, false);
        return new CheckoutItemHolder(view);
    }

    @Override
    public void onBindViewHolder(CheckoutItemHolder checkoutItemHolder, int position) {
        StringBuilder itemInformation = new StringBuilder();
        itemInformation.append(this.cartItemDataObjects.get(position).getItemName());
        itemInformation.append("\n");
        itemInformation.append(this.cartItemDataObjects.get(position).getItemWeightOrQuantity());
        itemInformation.append(" | ");
        itemInformation.append(
                checkoutItemHolder.getItemNameInformation().getContext().getString(
                        R.string.currency,
                        ItemCheckoutUtility.getInstance().getDiscountedPrice(
                                this.cartItemDataObjects.get(position).getItemPrice(),
                                this.cartItemDataObjects.get(position).getItemDiscount()
                        )
                )
        );
        checkoutItemHolder.getItemNameInformation().setText(
                itemInformation
        );
        checkoutItemHolder.getItemQuantityText().setText(
                Integer.toString(this.cartItemDataObjects.get(position).getQuantityToCheckout())
        );
        this.checkoutItemHolders.add(checkoutItemHolder);
        checkoutItemHolder.setCartItemDataObject(this.cartItemDataObjects.get(position));
        checkoutItemHolder.setCheckoutItemListeners();
    }

    @Override
    public int getItemCount() {
        return this.cartItemDataObjects.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public CheckoutItemHolder getCheckoutItemHolder(int position) {
        return this.checkoutItemHolders.get(position);
    }

    public int getCheckoutItemHolderSize() {
        return this.checkoutItemHolders.size();
    }

    public void removeCheckoutItemHolder(int position) {
        this.checkoutItemHolders.remove(position);
    }
}
