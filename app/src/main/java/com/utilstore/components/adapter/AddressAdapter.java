package com.utilstore.components.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.utilstore.app.R;
import com.utilstore.components.dataobjects.AddressDataObject;
import com.utilstore.components.viewholder.AddressHolder;

import java.util.List;

/**
 * Created by anfal on 1/5/2016.
 */
public class AddressAdapter extends RecyclerView.Adapter<AddressHolder> {

    private List<AddressDataObject> addressDataObjects = null;

    public AddressAdapter(List<AddressDataObject> addressDataObjects) {
        this.addressDataObjects = addressDataObjects;
    }

    @Override
    public AddressHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.myaccount_address_card, viewGroup, false);
        return new AddressHolder(view);
    }

    @Override
    public void onBindViewHolder(AddressHolder addressHolder, int position) {
        addressHolder.getLabel().setText(this.addressDataObjects.get(position).getLabel());
        addressHolder.setAddressDataObject(this.addressDataObjects.get(position));
        addressHolder.addEditDeleteListeners();
    }

    @Override
    public int getItemCount() {
        return this.addressDataObjects.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
