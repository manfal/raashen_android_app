package com.utilstore.components.adapter;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.utilstore.app.R;
import com.utilstore.components.dataobjects.DepartmentItemDataObject;
import com.utilstore.components.observers.CartObserver;
import com.utilstore.components.viewholder.DepartmentItemHolder;
import com.utilstore.constants.Config;
import com.utilstore.utilities.ItemCheckoutUtility;

import java.util.List;

/**
 * Created by anfal on 12/20/2015.
 */
public class DepartmentItemAdapter extends RecyclerView.Adapter<DepartmentItemHolder> {
    private List<DepartmentItemDataObject> departmentItemDataObjects = null;

    public DepartmentItemAdapter(List<DepartmentItemDataObject> departmentItemDataObjects) {
        this.departmentItemDataObjects = departmentItemDataObjects;
    }

    @Override
    public DepartmentItemHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_card, viewGroup, false);
        return new DepartmentItemHolder(view);
    }

    @Override
    public void onBindViewHolder(DepartmentItemHolder departmentItemHolder, int position) {
        departmentItemHolder.getItemNameTextView().setText(this.departmentItemDataObjects.get(position).getItemName());
        if (0 < this.departmentItemDataObjects.get(position).getItemDiscount()) {
            departmentItemHolder.getItemDiscountTextView().setText(
                    departmentItemHolder.getItemDiscountTextView().getContext().getString(
                            R.string.discount_on_item,
                            this.departmentItemDataObjects.get(position).getItemDiscount()
                    )
            );
            departmentItemHolder.getItemPriceTexView().setTextColor(
                    ContextCompat.getColor(departmentItemHolder.getItemPriceTexView().getContext(), R.color.green_200)
            );
            departmentItemHolder.getItemPriceTexView().setText(
                    departmentItemHolder.getItemPriceTexView().getContext().getString(
                            R.string.currency,
                            ItemCheckoutUtility.getInstance().getDiscountedPrice(
                                    this.departmentItemDataObjects.get(position).getItemPrice(),
                                    this.departmentItemDataObjects.get(position).getItemDiscount()
                            )
                    )
            );
        } else {
            departmentItemHolder.getItemPriceTexView().setText(
                    departmentItemHolder.getItemPriceTexView().getContext().getString(
                            R.string.currency,
                            this.departmentItemDataObjects.get(position).getItemPrice()
                    )
            );
            departmentItemHolder.getItemPriceTexView().setTextColor(
                    ContextCompat.getColor(departmentItemHolder.getItemPriceTexView().getContext(), R.color.black_400)
            );
            departmentItemHolder.getItemDiscountTextView().setText(null);
        }
        departmentItemHolder.getItemWeightTextView().setText(this.departmentItemDataObjects.get(position).getItemWeightOrQuantity());
        departmentItemHolder.setDepartmentItemDataObject(this.departmentItemDataObjects.get(position));
        Glide.with(departmentItemHolder.getImageView().getContext())
                .load(Config.API_HOST + this.departmentItemDataObjects.get(position).getItemImageThumbnailUrl())
                .fitCenter()
                .crossFade()
                .into(departmentItemHolder.getImageView());
        departmentItemHolder.getAddToCartSpinner().setSelection(
                CartObserver.getCartObserverInstance().getQuantityToCheckoutFromMap(this.departmentItemDataObjects.get(position).getItemId()),
                false
        );
        departmentItemHolder.setViewListeners();
        CartObserver.getCartObserverInstance().addToDepartmentItemList(departmentItemHolder);
    }

    @Override
    public int getItemCount() {
        return this.departmentItemDataObjects.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
