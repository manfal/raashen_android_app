package com.utilstore.components.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.utilstore.app.R;
import com.utilstore.components.dataobjects.StoreDepartmentDataObject;
import com.utilstore.components.viewholder.StoreDepartmentHolder;
import com.utilstore.constants.Config;

import java.util.List;

/**
 * Created by anfal on 12/17/2015.
 */
public class StoreDepartmentsAdapter extends RecyclerView.Adapter<StoreDepartmentHolder> {
    private List<StoreDepartmentDataObject> storeDepartmentDataObjects = null;

    public StoreDepartmentsAdapter(List<StoreDepartmentDataObject> storeDepartmentDataObjects) {
        this.storeDepartmentDataObjects = storeDepartmentDataObjects;
    }

    @Override
    public StoreDepartmentHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.department_card, viewGroup, false);
        return new StoreDepartmentHolder(view);
    }

    @Override
    public void onBindViewHolder(StoreDepartmentHolder storeDepartmentHolder, int position) {
        storeDepartmentHolder.getTextView().setText(this.storeDepartmentDataObjects.get(position).getDepartmentName());
        Glide.with(storeDepartmentHolder.getImageView().getContext())
                .load(Config.API_HOST + this.storeDepartmentDataObjects.get(position).getDepartmentImageUrl())
                .crossFade()
                .into(storeDepartmentHolder.getImageView());
        storeDepartmentHolder.setDepartmentPosition(position);
    }

    @Override
    public int getItemCount() {
        return this.storeDepartmentDataObjects.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public List<StoreDepartmentDataObject> getStoreItemList() {
        return this.storeDepartmentDataObjects;
    }
}
