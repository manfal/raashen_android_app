package com.utilstore.components.navigation;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.utilstore.app.R;
import com.utilstore.components.adapter.OrderHistoryAdapter;
import com.utilstore.components.dataobjects.OrderHistoryDataObject;

import java.util.List;

/**
 * Created by anfal on 1/8/2016.
 */
public class OrderHistoryList {
    private RecyclerView recyclerView = null;
    private Context context = null;
    private OrderHistoryAdapter orderHistoryAdapter = null;
    private List<OrderHistoryDataObject> orderHistoryDataObjects = null;

    public OrderHistoryList(RecyclerView recyclerView, Context context) {
        this.context = context;
        this.recyclerView = recyclerView;
    }

    public boolean populateOrderHistory(List<OrderHistoryDataObject> orderHistoryDataObjects) throws Exception {
        this.orderHistoryDataObjects = orderHistoryDataObjects;
        this.orderHistoryAdapter = new OrderHistoryAdapter(this.orderHistoryDataObjects);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.context);
        this.recyclerView.setLayoutManager(linearLayoutManager);
        this.recyclerView.setAdapter(this.orderHistoryAdapter);
        Animation animation = AnimationUtils.loadAnimation(this.context, R.anim.store_department_animation);
        this.recyclerView.startAnimation(animation);
        return true;
    }

    public OrderHistoryAdapter getOrderHistoryAdapter() {
        return this.orderHistoryAdapter;
    }

    public List<OrderHistoryDataObject> getOrderHistoryDataObjects() {
        return this.orderHistoryDataObjects;
    }
}
