package com.utilstore.components.navigation;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.utilstore.app.R;
import com.utilstore.components.adapter.StoreDepartmentsAdapter;
import com.utilstore.components.dataobjects.StoreDepartmentDataObject;

import java.util.List;

/**
 * Created by anfal on 12/17/2015.
 */
public class StoreMenu {
    private RecyclerView recyclerView = null;
    private AppCompatActivity appCompatActivity = null;
    private StoreDepartmentsAdapter storeDepartmentsAdapter = null;

    public StoreMenu(RecyclerView recyclerView, AppCompatActivity appCompatActivity) {
        this.recyclerView = recyclerView;
        this.appCompatActivity = appCompatActivity;
    }

    public boolean instantiateStoreMenu(List<StoreDepartmentDataObject> storeDepartmentDataObjects) throws Exception {
        this.storeDepartmentsAdapter = new StoreDepartmentsAdapter(storeDepartmentDataObjects);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.appCompatActivity);
        this.recyclerView.setLayoutManager(linearLayoutManager);
        this.recyclerView.setAdapter(this.storeDepartmentsAdapter);
        Animation animation = AnimationUtils.loadAnimation(this.appCompatActivity, R.anim.store_department_animation);
        this.recyclerView.startAnimation(animation);
        return true;
    }

    public StoreDepartmentsAdapter getStoreDepartmentsAdapter() {
        return this.storeDepartmentsAdapter;
    }

    public RecyclerView getRecyclerView() {
        return this.recyclerView;
    }
}
