package com.utilstore.components.navigation;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.utilstore.app.R;
import com.utilstore.components.adapter.CartItemAdapter;
import com.utilstore.components.dataobjects.CartItemDataObject;

import java.util.List;

/**
 * Created by anfal on 1/13/2016.
 */
public class Cart {

    private List<CartItemDataObject> cartItemDataObjects = null;
    private RecyclerView recyclerView = null;
    private Context context = null;
    private CartItemAdapter cartItemAdapter = null;

    public Cart(RecyclerView recyclerView, Context context) {
        this.context = context;
        this.recyclerView = recyclerView;
    }

    public boolean populateCart(List<CartItemDataObject> cartItemDataObjects) throws Exception {
        this.cartItemDataObjects = cartItemDataObjects;
        this.cartItemAdapter = new CartItemAdapter(this.cartItemDataObjects);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.context);
        this.recyclerView.setLayoutManager(linearLayoutManager);
        this.recyclerView.setAdapter(this.cartItemAdapter);
        Animation animation = AnimationUtils.loadAnimation(this.context, R.anim.store_department_animation);
        this.recyclerView.startAnimation(animation);
        return true;
    }

    public List<CartItemDataObject> getCartItemDataObjects() {
        return this.cartItemDataObjects;
    }

    public CartItemAdapter getCartItemAdapter() {
        return this.cartItemAdapter;
    }
}
