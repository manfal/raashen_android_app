package com.utilstore.components.navigation;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.utilstore.app.R;
import com.utilstore.components.adapter.AddressAdapter;
import com.utilstore.components.dataobjects.AddressDataObject;

import java.util.List;

/**
 * Created by anfal on 1/5/2016.
 */
public class Addresses {
    private List<AddressDataObject> addressDataObjects = null;
    private RecyclerView recyclerView = null;
    private Context context = null;
    private AddressAdapter addressAdapter = null;

    public Addresses(RecyclerView recyclerView, Context context) {
        this.context = context;
        this.recyclerView = recyclerView;
    }

    public boolean populateAddresses(List<AddressDataObject> addressDataObjects) throws Exception {
        this.addressDataObjects = addressDataObjects;
        this.addressAdapter = new AddressAdapter(this.addressDataObjects);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.context);
        this.recyclerView.setLayoutManager(linearLayoutManager);
        this.recyclerView.setAdapter(this.addressAdapter);
        Animation animation = AnimationUtils.loadAnimation(this.context, R.anim.store_department_animation);
        this.recyclerView.startAnimation(animation);
        return true;
    }

    public List<AddressDataObject> getAddressDataObjects() {
        return this.addressDataObjects;
    }

    public AddressAdapter getAddressAdapter() {
        return this.addressAdapter;
    }
}
