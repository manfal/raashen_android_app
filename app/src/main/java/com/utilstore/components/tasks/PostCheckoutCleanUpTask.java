package com.utilstore.components.tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.utilstore.actions.CheckoutPageActions;
import com.utilstore.analytics.logging.Constants;
import com.utilstore.analytics.logging.ILog;
import com.utilstore.analytics.logging.Logger;
import com.utilstore.database.ClientDB;

/**
 * Created by anfal on 1/29/2016.
 */
public class PostCheckoutCleanUpTask extends AsyncTask<Void, Void, Void> {

    ClientDB clientDB = null;
    private ILog logger = null;

    public PostCheckoutCleanUpTask(ClientDB clientDB) {
        this.clientDB = clientDB;
        this.logger = Logger.getLogger();
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {
            this.clientDB.deleteOrderHistoryRecords();
            Thread.sleep(2000);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        CheckoutPageActions.getCheckoutPageActionsInstance().goBackToStore();
    }
}