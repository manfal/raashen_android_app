package com.utilstore.components.interfaces;

import org.json.JSONObject;

/**
 * Created by anfal on 1/28/2016.
 */
public interface IAddressResponseSenderReceiver {
    void openNewAddressDialog();

    void sendNewAddressRequest(String label, String address, String city);

    void receiveNewAddressResponse(JSONObject response);
}
