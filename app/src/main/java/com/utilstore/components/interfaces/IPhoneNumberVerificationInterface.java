package com.utilstore.components.interfaces;

import org.json.JSONObject;

/**
 * Created by anfal on 1/28/2016.
 */
public interface IPhoneNumberVerificationInterface {
    void handleVerifyPhoneNumberLinkClick();

    void handlePhoneNumberVerification(String verificationCode);

    void postVerificationChores(JSONObject jsonObject);
}
