package com.utilstore.components.interfaces;

/**
 * Created by anfal on 2/6/2016.
 */
public interface IInternetConnectionObject {
    void startReconnecting();

    void tryToReconnect();
}
