package com.utilstore.components.interfaces;

/**
 * Created by anfal on 2/6/2016.
 */
public interface IInternetConnectionSubject {
    void registerAsObserver(IInternetConnectionObject internetConnectionObject);

    void unRegisterAsObserver(IInternetConnectionObject internetConnectionObject);

    void updateObserver();
}
