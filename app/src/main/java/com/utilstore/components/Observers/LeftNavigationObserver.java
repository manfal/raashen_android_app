package com.utilstore.components.observers;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.utilstore.actions.CustomerHelpActions;
import com.utilstore.actions.DepartmentActions;
import com.utilstore.analytics.LogEvents;
import com.utilstore.analytics.logging.Constants;
import com.utilstore.analytics.logging.ILog;
import com.utilstore.analytics.logging.Logger;
import com.utilstore.app.CustomerHelpPage;
import com.utilstore.app.DepartmentPage;
import com.utilstore.app.LoginPage;
import com.utilstore.app.MyAccountPage;
import com.utilstore.app.OrderHistoryPage;
import com.utilstore.app.R;
import com.utilstore.app.StorePage;
import com.utilstore.components.adapter.DrawerAdapter;
import com.utilstore.components.dataobjects.ChildDataObject;
import com.utilstore.components.dataobjects.ParentDataObjectListener;
import com.utilstore.components.listeners.ToolBarNavigationClickListener;
import com.utilstore.components.navigation.LeftMenu;
import com.utilstore.database.ClientDB;
import com.utilstore.datastore.LeftNavigationObserverController;
import com.utilstore.utilities.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.utilstore.constants.Constants.CATEGORY_ID;
import static com.utilstore.constants.Constants.CATEGORY_NAME;
import static com.utilstore.constants.Constants.CONTACT_NAVIGATION;
import static com.utilstore.constants.Constants.DEPARTMENT_CATEGORIES;
import static com.utilstore.constants.Constants.DEPARTMENT_IDENTIFIER;
import static com.utilstore.constants.Constants.DEPARTMENT_NAME;
import static com.utilstore.constants.Constants.HELP_NAVIGATION;
import static com.utilstore.constants.Constants.LOCATIONS_NAVIGATION;
import static com.utilstore.constants.Constants.PRIVACY_POLICY_NAVIGATION;
import static com.utilstore.constants.Constants.TERMS_AND_CONDITIONS_NAVIGATION;
import static com.utilstore.constants.Constants.pageName;

/**
 * Created by anfal on 1/15/2016.
 */
public class LeftNavigationObserver {

    private static LeftNavigationObserver leftNavigationObserver = null;
    private ILog logger = null;
    private LeftMenu leftMenu = null;
    private AppCompatActivity appCompatActivity = null;
    private DrawerLayout drawerLayout = null;
    private LeftNavigationObserverController leftNavigationObserverController = null;
    private ClientDB clientDB = null;

    public LeftNavigationObserver(AppCompatActivity appCompatActivity, DrawerLayout drawerLayout, Toolbar toolbar) {
        this.logger = Logger.getLogger();
        try {
            this.appCompatActivity = appCompatActivity;
            this.clientDB = new ClientDB(this.appCompatActivity);
            RecyclerView leftNavRecyclerView = (RecyclerView) this.appCompatActivity.findViewById(R.id.left_menu);
            this.drawerLayout = drawerLayout;
            this.leftMenu = new LeftMenu(leftNavRecyclerView, this.appCompatActivity).bindDrawerToggle(this.drawerLayout, toolbar);
            this.leftNavigationObserverController = new LeftNavigationObserverController(this.appCompatActivity);
            this.fetfchLeftNavigation();
            toolbar.setNavigationOnClickListener(new ToolBarNavigationClickListener(this.drawerLayout));
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public static void setLeftNavigationObserverInstance(AppCompatActivity appCompatActivity, DrawerLayout drawerLayout, Toolbar toolbar) {
        leftNavigationObserver = new LeftNavigationObserver(appCompatActivity, drawerLayout, toolbar);
    }

    public static LeftNavigationObserver getLeftNavigationObserverInstance() {
        return leftNavigationObserver;
    }

    public void populateLeftMenu(JSONArray itemsArray) {
        try {
            String[] defaultItemArray = Utilities.getInstance().getDefaultMenuArray(this.appCompatActivity);
            this.leftMenu.instantiateMenu(this.parseLeftNav(itemsArray, defaultItemArray));
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void navigationItemClickListener(int position) {
        try {
            DrawerAdapter drawerAdapter = this.leftMenu.getExpandableAdapter();
            int currentNavigationPosition = this.leftMenu.getCurrentNavItemPosition();
            int categoryPosition = (position - currentNavigationPosition) - 1;
            ParentDataObjectListener parentDataObject = (ParentDataObjectListener) drawerAdapter.getParentItemList().get(currentNavigationPosition);
            ChildDataObject childDataObject = (ChildDataObject) drawerAdapter.getParentItemList().get(currentNavigationPosition).getChildItemList().get(categoryPosition);
            FlurryAgent.logEvent(
                    LogEvents.EVENT_ID_MAIN_STORE_DEPARTMENT,
                    LogEvents.getEventData(
                            LogEvents.EVENT_ID_MAIN_STORE_DEPARTMENT,
                            new String[]{
                                    LogEvents.TRIGGERED_LEFT_NAV_CATEGORY_ITEM_EVENT,
                                    parentDataObject.getParentText(),
                                    childDataObject.getChildText()
                            }
                    )
            );
            Utilities.getInstance().resetStaticActivityData();
            if (this.appCompatActivity instanceof DepartmentPage) {
                if (parentDataObject.getParentId() == DepartmentActions.getDepartmentActionInstance().getDepartmentId()) {
                    DepartmentActions.getDepartmentActionInstance().selectCurrentlyActiveTab(childDataObject.getChildText());
                } else {
                    DepartmentActions.getDepartmentActionInstance().fetchCategoryData(
                            parentDataObject.getParentText(),
                            parentDataObject.getParentId(),
                            childDataObject.getChildId()
                    );
                }
            } else {
                Utilities.getInstance().startDepartmentActivity(this.appCompatActivity, parentDataObject.getParentText(), parentDataObject.getParentId(), childDataObject.getChildId());
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void navigationItemDefaultMenuListener(int position, String navigationText) {
        try {
            switch (navigationText) {
                case "Store":
                    if (this.appCompatActivity instanceof StorePage) {
                        if (this.drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                            this.drawerLayout.closeDrawer(Gravity.LEFT);
                        }
                    } else {
                        Utilities.getInstance().startNewActivityByFinishingOlder(this.appCompatActivity, StorePage.class);
                    }
                    break;
                case "My Account":
                    if (this.appCompatActivity instanceof MyAccountPage) {
                        if (this.drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                            this.drawerLayout.closeDrawer(Gravity.LEFT);
                        }
                    } else {
                        Utilities.getInstance().startNewActivityByFinishingOlder(this.appCompatActivity, MyAccountPage.class);
                    }
                    break;
                case "Order History":
                    if (this.appCompatActivity instanceof OrderHistoryPage) {
                        if (this.drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                            this.drawerLayout.closeDrawer(Gravity.LEFT);
                        }
                    } else {
                        Utilities.getInstance().startNewActivityByFinishingOlder(this.appCompatActivity, OrderHistoryPage.class);
                    }
                    break;
                case HELP_NAVIGATION:
                case LOCATIONS_NAVIGATION:
                case TERMS_AND_CONDITIONS_NAVIGATION:
                case CONTACT_NAVIGATION:
                case PRIVACY_POLICY_NAVIGATION:
                    pageName = navigationText;
                    FlurryAgent.logEvent(
                            LogEvents.EVENT_ID_MAIN_CUSTOMER_HELP,
                            LogEvents.getEventData(
                                    LogEvents.EVENT_ID_MAIN_CUSTOMER_HELP,
                                    new String[]{
                                            LogEvents.TRIGGERED_CUSTOMER_HELP_EVENT,
                                            String.valueOf(position)
                                    }
                            )
                    );
                    if (this.appCompatActivity instanceof CustomerHelpPage) {
                        if (this.drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                            CustomerHelpActions.getCustomerHelpActions().loadNewWebPage(navigationText);
                            this.drawerLayout.closeDrawer(Gravity.LEFT);
                        }
                    } else {
                        Utilities.getInstance().startCustomerHelpActivity(this.appCompatActivity, navigationText);
                    }
                    break;
                case "Logout":
                    //Updating the value for email will make sure that email delivery charges info is not deleted
                    //Because user can use cart mechanism even after logout.
                    this.clientDB.updateUserEmailToDefaultForLogout();
                    Utilities.getInstance().startNewActivityByFinishingOlder(this.appCompatActivity, LoginPage.class);
                    break;
                case "Login":
                    Utilities.getInstance().startNewActivityByFinishingOlder(this.appCompatActivity, LoginPage.class);
                    break;
                default:
                    Toast.makeText(this.appCompatActivity, R.string.could_not_recognize_menu_item_text, Toast.LENGTH_LONG).show();
                    break;
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    private List<ParentDataObjectListener> parseLeftNav(JSONArray departmentArray, String[] defaultItemArray) throws JSONException {
        List<ParentDataObjectListener> departments = new ArrayList<>();
        int departmentSize = departmentArray.length();
        for (int departmentNumber = 0; departmentNumber < departmentArray.length(); departmentNumber++) {
            JSONObject departmentObject = departmentArray.getJSONObject(departmentNumber);
            List<ChildDataObject> categories = new ArrayList<>();
            JSONArray categoryArray = departmentObject.getJSONArray(DEPARTMENT_CATEGORIES);
            for (int categoryNumber = 0; categoryNumber < categoryArray.length(); categoryNumber++) {
                ChildDataObject category = new ChildDataObject();
                JSONObject categoryObject = categoryArray.getJSONObject(categoryNumber);
                category.setChildText(categoryObject.getString(CATEGORY_NAME));
                category.setChildId(Integer.parseInt(categoryObject.getString(CATEGORY_ID)));
                categories.add(category);
            }
            ParentDataObjectListener department = new ParentDataObjectListener();
            department.setChildItemList(categories);
            department.setParentNumber(departmentNumber);
            department.setParentId(Integer.parseInt(departmentObject.getString(DEPARTMENT_IDENTIFIER)));
            department.setParentText(departmentObject.getString(DEPARTMENT_NAME));
            if (departmentNumber == 0) {
                department.setInitiallyExpanded(false);
            }
            departments.add(department);
        }

        for (int defaultItemNumber = 0; defaultItemNumber < defaultItemArray.length; defaultItemNumber++) {
            ParentDataObjectListener defaultNavItem = new ParentDataObjectListener();
            defaultNavItem.setParentNumber(departmentSize++);
            defaultNavItem.setParentId((defaultItemNumber + 1) * -1);
            defaultNavItem.setParentText(defaultItemArray[defaultItemNumber]);
            departments.add(defaultNavItem);
        }

        return departments;
    }

    public void fetfchLeftNavigation() {
        try {
            this.leftNavigationObserverController.getLeftMenu(this);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }
}
