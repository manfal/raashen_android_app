package com.utilstore.components.dataobjects;

/**
 * Created by anfal on 12/17/2015.
 */
public class StoreDepartmentDataObject {
    private String departmentName = null;
    private int departmentId = 0;
    private String imageUrl = null;

    public StoreDepartmentDataObject(String departmentName, int departmentId, String imageUrl) {
        this.departmentName = departmentName;
        this.departmentId = departmentId;
        this.imageUrl = imageUrl;
    }

    public String getDepartmentName() {
        return this.departmentName;
    }

    public int getDepartmentId() {
        return this.departmentId;
    }

    public String getDepartmentImageUrl() {
        return this.imageUrl;
    }
}
