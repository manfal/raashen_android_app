package com.utilstore.components.dataobjects;

/**
 * Created by anfal on 12/5/2015.
 */
public class ChildDataObject {
    private String mChildText;
    private int mChildId;

    public String getChildText() {
        return mChildText;
    }

    public void setChildText(String childText) {
        mChildText = childText;
    }

    public int getChildId() {
        return mChildId;
    }

    public void setChildId(int id) {
        mChildId = id;
    }
}
