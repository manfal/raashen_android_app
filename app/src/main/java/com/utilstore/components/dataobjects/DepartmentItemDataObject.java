package com.utilstore.components.dataobjects;

/**
 * Created by anfal on 12/20/2015.
 */
public class DepartmentItemDataObject {
    private Integer itemPrice, itemDiscount, itemId, orderedQuantity = 0;
    private String itemImageUrl, itemImageThumbnailUrl, itemWeightOrQuantity, nutritionFacts, itemDescription, itemName = null;

    public int getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(int itemPrice) {
        this.itemPrice = itemPrice;
    }

    public int getItemDiscount() {
        return itemDiscount;
    }

    public void setItemDiscount(int itemDiscount) {
        this.itemDiscount = itemDiscount;
    }

    public String getItemImageUrl() {
        return itemImageUrl;
    }

    public void setItemImageUrl(String itemImageUrl) {
        this.itemImageUrl = itemImageUrl;
    }

    public String getItemImageThumbnailUrl() {
        return itemImageThumbnailUrl;
    }

    public void setItemImageThumbnailUrl(String itemImageThumbnailUrl) {
        this.itemImageThumbnailUrl = itemImageThumbnailUrl;
    }

    public String getItemWeightOrQuantity() {
        return itemWeightOrQuantity;
    }

    public void setItemWeightOrQuantity(String itemWeightOrQuantity) {
        this.itemWeightOrQuantity = itemWeightOrQuantity;
    }

    public String getNutritionFacts() {
        return nutritionFacts;
    }

    public void setNutritionFacts(String nutritionFacts) {
        this.nutritionFacts = nutritionFacts;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getOrderedQuantity() {
        return orderedQuantity;
    }

    public void setOrderedQuantity(int orderedQuantity) {
        this.orderedQuantity = orderedQuantity;
    }
}
