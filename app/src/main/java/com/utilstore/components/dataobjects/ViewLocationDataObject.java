package com.utilstore.components.dataobjects;

/**
 * Created by anfal on 12/29/2015.
 */
public class ViewLocationDataObject {
    private int x, y = 0;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
