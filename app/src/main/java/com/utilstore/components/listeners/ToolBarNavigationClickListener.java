package com.utilstore.components.listeners;

import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;

import com.utilstore.analytics.logging.Constants;
import com.utilstore.analytics.logging.ILog;
import com.utilstore.analytics.logging.Logger;

/**
 * Created by anfal on 12/19/2015.
 */
public class ToolBarNavigationClickListener implements View.OnClickListener {

    private DrawerLayout drawerLayout = null;
    private ILog logger = null;

    public ToolBarNavigationClickListener(DrawerLayout drawerLayout) {
        this.drawerLayout = drawerLayout;
        this.logger = Logger.getLogger();
    }

    @Override
    public void onClick(View v) {
        try {
            if (!this.drawerLayout.isDrawerOpen(Gravity.LEFT) && !this.drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                this.drawerLayout.openDrawer(Gravity.LEFT);
            } else if (this.drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                this.drawerLayout.closeDrawer(Gravity.LEFT);
            } else if (this.drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                this.drawerLayout.closeDrawer(Gravity.RIGHT);
            }
        } catch (Exception ex) {
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }
}
