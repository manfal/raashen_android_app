package com.utilstore.components.listeners;

import android.view.View;

import com.utilstore.actions.PersonalInfoFragmentActions;

/**
 * Created by anfal on 1/2/2016.
 */
public class MyAccountEditPasswordListener implements View.OnClickListener {
    @Override
    public void onClick(View v) {
        PersonalInfoFragmentActions.getPersonalInfoActionInstance().handlePasswordEditHyperLinkClick();
    }
}
