package com.utilstore.components.listeners;

import android.view.View;

import com.utilstore.actions.PersonalAddressFragmentActionsSender;
import com.utilstore.components.dataobjects.AddressDataObject;

/**
 * Created by anfal on 1/6/2016.
 */
public class AddressDeleteListener implements View.OnClickListener {

    private AddressDataObject addressDataObject = null;

    public AddressDeleteListener(AddressDataObject addressDataObject) {
        this.addressDataObject = addressDataObject;
    }

    @Override
    public void onClick(View v) {
        PersonalAddressFragmentActionsSender.getPersonalAddressFragmentActions().deletAddress(addressDataObject);
    }
}
