package com.utilstore.components.listeners;

import android.view.View;

import com.utilstore.actions.PersonalAddressFragmentActionsSender;
import com.utilstore.components.viewholder.AddressHolder;

/**
 * Created by anfal on 1/6/2016.
 */
public class AddressEditListener implements View.OnClickListener {

    private AddressHolder addressHolder = null;

    public AddressEditListener(AddressHolder addressHolder) {
        this.addressHolder = addressHolder;
    }

    @Override
    public void onClick(View v) {
        PersonalAddressFragmentActionsSender.getPersonalAddressFragmentActions().editAddress(this.addressHolder);
    }
}
