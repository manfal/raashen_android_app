package com.utilstore.components.listeners;

import android.support.v4.view.ViewPager;

import com.utilstore.actions.DepartmentActions;

/**
 * Created by anfal on 12/21/2015.
 */
public class DepartmentCategoryPagerListener implements ViewPager.OnPageChangeListener {

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        DepartmentActions.getDepartmentActionInstance().checkInternetConnectivityAgain(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }
}

