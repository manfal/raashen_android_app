package com.utilstore.components.listeners;

import java.util.List;

public interface ParentListItemListener {

    List<?> getChildItemList();

    boolean isInitiallyExpanded();
}