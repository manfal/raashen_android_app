package com.utilstore.components.listeners;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.utilstore.analytics.logging.Constants;
import com.utilstore.analytics.logging.ILog;
import com.utilstore.analytics.logging.Logger;
import com.utilstore.app.CheckoutPage;
import com.utilstore.app.LoginPage;
import com.utilstore.utilities.Utilities;

import static com.utilstore.constants.Constants.shouldUserBeRedirectedToCheckoutPage;

/**
 * Created by anfal on 1/27/2016.
 */
public class CheckoutButtonClickListener implements View.OnClickListener {

    private AppCompatActivity appCompatActivity = null;
    private ILog logger = null;

    public CheckoutButtonClickListener(AppCompatActivity appCompatActivity) {
        this.appCompatActivity = appCompatActivity;
        this.logger = Logger.getLogger();
    }

    @Override
    public void onClick(View v) {
        try {
            if (Utilities.getInstance().isSessionNotEmpty(this.appCompatActivity)) {
                Utilities.getInstance().startNewActivityByFinishingOlder(this.appCompatActivity, CheckoutPage.class);
            } else {
                shouldUserBeRedirectedToCheckoutPage = true;
                Utilities.getInstance().startNewActivityByFinishingOlder(this.appCompatActivity, LoginPage.class);
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }
}
