package com.utilstore.components.listeners;

import android.view.View;
import android.widget.Button;

import com.utilstore.actions.CheckoutPageActions;

/**
 * Created by anfal on 1/27/2016.
 */
public class CheckoutPageButtonListener implements View.OnClickListener {

    @Override
    public void onClick(View v) {
        Button clickedButton = (Button) v;
        CheckoutPageActions.getCheckoutPageActionsInstance().checkoutButtonListenEvent(clickedButton.getText().toString());
    }

}
