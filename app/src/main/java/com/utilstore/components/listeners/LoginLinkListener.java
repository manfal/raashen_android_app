package com.utilstore.components.listeners;

import android.view.View;

import com.utilstore.actions.ResetPasswordActions;
import com.utilstore.actions.SignupActions;

/**
 * Created by anfal on 12/25/2015.
 */
public class LoginLinkListener implements View.OnClickListener {
    @Override
    public void onClick(View v) {
        if (null != ResetPasswordActions.getResetPasswordActionsInstance()) {
            ResetPasswordActions.getResetPasswordActionsInstance().startLoginActivity();
        } else if (null != SignupActions.getSignupActionsInstance()) {
            SignupActions.getSignupActionsInstance().startLoginActivity();
        }
    }
}
