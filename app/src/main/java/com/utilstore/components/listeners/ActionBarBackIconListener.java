package com.utilstore.components.listeners;

import android.util.Log;
import android.view.View;

import com.utilstore.actions.SearchActions;
import com.utilstore.analytics.logging.Constants;
import com.utilstore.analytics.logging.ILog;
import com.utilstore.analytics.logging.Logger;

/**
 * Created by anfal on 12/30/2015.
 */
public class ActionBarBackIconListener implements View.OnClickListener {

    private ILog logger = null;

    public ActionBarBackIconListener() {
        this.logger = Logger.getLogger();
    }

    @Override
    public void onClick(View v) {
        try {
            SearchActions.getSearchActionsInstance().finishSearchActivity();
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }
}
