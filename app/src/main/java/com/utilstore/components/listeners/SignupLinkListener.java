package com.utilstore.components.listeners;

import android.view.View;

import com.utilstore.actions.LoginActions;

/**
 * Created by anfal on 12/25/2015.
 */
public class SignupLinkListener implements View.OnClickListener {
    @Override
    public void onClick(View v) {
        LoginActions.getLoginActionsInstance().startSignupActivity();
    }
}
