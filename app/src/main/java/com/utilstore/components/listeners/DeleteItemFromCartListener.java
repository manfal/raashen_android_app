package com.utilstore.components.listeners;

import android.view.View;

import com.utilstore.components.dataobjects.CartItemDataObject;
import com.utilstore.components.observers.CartObserver;

/**
 * Created by anfal on 1/13/2016.
 */
public class DeleteItemFromCartListener implements View.OnClickListener {

    private CartItemDataObject cartItemDataObject = null;

    public DeleteItemFromCartListener(CartItemDataObject cartItemDataObject) {
        this.cartItemDataObject = cartItemDataObject;
    }

    @Override
    public void onClick(View v) {
        CartObserver.getCartObserverInstance().deleteCartItem(this.cartItemDataObject);
    }

}
