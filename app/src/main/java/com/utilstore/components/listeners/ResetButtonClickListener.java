package com.utilstore.components.listeners;

import android.view.View;

import com.utilstore.actions.ResetPasswordActions;

/**
 * Created by anfal on 12/25/2015.
 */
public class ResetButtonClickListener implements View.OnClickListener {
    @Override
    public void onClick(View v) {
        ResetPasswordActions.getResetPasswordActionsInstance().resetPassword();
    }
}
