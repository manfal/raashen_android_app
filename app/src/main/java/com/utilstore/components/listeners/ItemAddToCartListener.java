package com.utilstore.components.listeners;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.utilstore.analytics.logging.Constants;
import com.utilstore.analytics.logging.ILog;
import com.utilstore.analytics.logging.Logger;
import com.utilstore.app.R;
import com.utilstore.components.dataobjects.DepartmentItemDataObject;
import com.utilstore.components.observers.CartObserver;
import com.utilstore.utilities.Utilities;

/**
 * Created by anfal on 1/18/2016.
 */
public class ItemAddToCartListener implements AdapterView.OnItemSelectedListener, View.OnTouchListener {

    private DepartmentItemDataObject departmentItemDataObject = null;
    private boolean userSelect = false;
    private ILog logger = null;
    private Spinner spinner = null;

    public ItemAddToCartListener(DepartmentItemDataObject departmentItemDataObject, Spinner spinner) {
        this.departmentItemDataObject = departmentItemDataObject;
        this.logger = Logger.getLogger();
        this.spinner = spinner;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        try {
            this.userSelect = true;
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
        return false;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        try {
            if (Utilities.getInstance().isInternetConnectionNotAvailable(view.getContext())) {
                this.spinner.setSelection(0);
                Toast.makeText(view.getContext(), view.getContext().getString(R.string.need_internet_to_change_item_quantity_text), Toast.LENGTH_LONG).show();
            } else {
                if (this.userSelect) {
                    this.userSelect = false;
                    CartObserver.getCartObserverInstance().addItemToCart(parent.getSelectedItem(), this.departmentItemDataObject);
                }
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
