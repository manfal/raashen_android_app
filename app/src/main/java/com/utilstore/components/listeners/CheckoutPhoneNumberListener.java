package com.utilstore.components.listeners;

import android.view.View;

import com.utilstore.actions.CheckoutPageActions;

/**
 * Created by anfal on 1/28/2016.
 */
public class CheckoutPhoneNumberListener implements View.OnClickListener {
    @Override
    public void onClick(View v) {
        CheckoutPageActions.getCheckoutPageActionsInstance().showPhoneNumerDialog();
    }
}
