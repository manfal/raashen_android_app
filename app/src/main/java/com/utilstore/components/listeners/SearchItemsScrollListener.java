package com.utilstore.components.listeners;

import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.utilstore.actions.SearchActions;
import com.utilstore.analytics.logging.ILog;
import com.utilstore.analytics.logging.Logger;
import com.utilstore.enumerations.EItemFetchChoice;

import static com.utilstore.analytics.logging.Constants.ERROR;
import static com.utilstore.analytics.logging.Constants.TAG_ERROR;

/**
 * Created by anfal on 12/31/2015.
 */
public class SearchItemsScrollListener extends RecyclerView.OnScrollListener {

    private ILog logger = null;

    public SearchItemsScrollListener() {
        this.logger = Logger.getLogger();
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        try {
            if (!recyclerView.canScrollVertically(1) && SearchActions.getSearchActionsInstance().isDataFetchNecessary()) {
                SearchActions.getSearchActionsInstance().fetchSearchItems(EItemFetchChoice.FETCHING_TO_APPEND);
            }
        } catch (Exception ex) {
            this.logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

}
