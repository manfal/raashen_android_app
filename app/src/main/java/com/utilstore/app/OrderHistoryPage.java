package com.utilstore.app;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;

import com.utilstore.actions.OrderHistoryActions;
import com.utilstore.analytics.logging.Constants;
import com.utilstore.analytics.logging.ILog;
import com.utilstore.analytics.logging.Logger;
import com.utilstore.customclasses.CustomCompleteToolBarMenuActivity;
import com.utilstore.enumerations.EItemFetchChoice;
import com.utilstore.utilities.Utilities;

/**
 * Created by anfal on 1/8/2016.
 */
public class OrderHistoryPage extends CustomCompleteToolBarMenuActivity {

    private ILog logger = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history_page);

        this.logger = Logger.getLogger();

        try {
            OrderHistoryActions.setOrderHistoryActionsInstance(this);
            super.assignDrawerLayout(OrderHistoryActions.getOrderHistoryActionsInstance().getNavigationDrawer());
            OrderHistoryActions.getOrderHistoryActionsInstance().fetchOrderHistory(EItemFetchChoice.FETCHING_FIRST_TIME);
        } catch (Exception ex) {
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        OrderHistoryActions.getOrderHistoryActionsInstance().destroyOrderHistoryActionsInstance();
    }

    @Override
    public void onBackPressed() {
        try {
            if (OrderHistoryActions.getOrderHistoryActionsInstance().getNavigationDrawer().isDrawerOpen(Gravity.RIGHT)) {
                OrderHistoryActions.getOrderHistoryActionsInstance().getNavigationDrawer().closeDrawer(Gravity.RIGHT);
            } else if (null != OrderHistoryActions.getOrderHistoryActionsInstance().getAnimater()) {
                OrderHistoryActions.getOrderHistoryActionsInstance().hideOrderHistoryDetailLayout();
            } else {
                Utilities.getInstance().startNewActivityByFinishingOlder(this, StorePage.class);
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }
}
