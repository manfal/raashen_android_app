package com.utilstore.app;

import android.os.Bundle;
import android.util.Log;

import com.utilstore.actions.SignupActions;
import com.utilstore.analytics.logging.Constants;
import com.utilstore.analytics.logging.ILog;
import com.utilstore.analytics.logging.Logger;
import com.utilstore.customclasses.CustomAccountActivity;

public class SignupPage extends CustomAccountActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_page);

        ILog logger = Logger.getLogger();
        try {
            super.setLogger(logger);
            SignupActions.setSignupActionsInstance(this);
        } catch (Exception ex) {
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SignupActions.getSignupActionsInstance().destroySignupActionsInstance();
    }
}
