package com.utilstore.app;

import android.os.Bundle;
import android.util.Log;

import com.utilstore.actions.ResetPasswordActions;
import com.utilstore.analytics.logging.Constants;
import com.utilstore.analytics.logging.ILog;
import com.utilstore.analytics.logging.Logger;
import com.utilstore.customclasses.CustomAccountActivity;

public class ResetPasswordPage extends CustomAccountActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password_page);

        ILog logger = Logger.getLogger();
        try {
            super.setLogger(logger);
            ResetPasswordActions.setResetPasswordActionsInstance(this);
        } catch (Exception ex) {
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ResetPasswordActions.getResetPasswordActionsInstance().destroyResetPasswordActionsInstance();
    }
}
