package com.utilstore.app;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;

import com.utilstore.actions.DepartmentActions;
import com.utilstore.analytics.logging.Constants;
import com.utilstore.analytics.logging.ILog;
import com.utilstore.analytics.logging.Logger;
import com.utilstore.customclasses.CustomCompleteToolBarMenuActivity;
import com.utilstore.utilities.Utilities;

import static com.utilstore.constants.Constants.NON_APPLICABLE_ID;

/**
 * Created by anfal on 12/19/2015.
 */
public class DepartmentPage extends CustomCompleteToolBarMenuActivity {

    private ILog logger = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_department_page);

        this.logger = Logger.getLogger();

        try {
            DepartmentActions.setDepartmentActionInstance(this);
            DepartmentActions.getDepartmentActionInstance().fetchCategoryData(null, NON_APPLICABLE_ID, NON_APPLICABLE_ID);
            super.assignDrawerLayout(DepartmentActions.getDepartmentActionInstance().getDepartmentDrawer());
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        DepartmentActions.getDepartmentActionInstance().destroyDepartmentActionInstance();
    }

    @Override
    public void onBackPressed() {
        try {
            if (DepartmentActions.getDepartmentActionInstance().getDepartmentDrawer().isDrawerOpen(Gravity.RIGHT)) {
                DepartmentActions.getDepartmentActionInstance().getDepartmentDrawer().closeDrawer(Gravity.RIGHT);
            } else if (null != DepartmentActions.getDepartmentActionInstance().getItemDetailLayoutManipulator().getAnimator()) {
                DepartmentActions.getDepartmentActionInstance().getItemDetailLayoutManipulator().hideItemDetails();
            } else {
                Utilities.getInstance().startNewActivityByFinishingOlder(this, StorePage.class);
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }
}
