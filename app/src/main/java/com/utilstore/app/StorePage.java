package com.utilstore.app;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;

import com.utilstore.actions.StoreActions;
import com.utilstore.analytics.logging.Constants;
import com.utilstore.analytics.logging.ILog;
import com.utilstore.analytics.logging.Logger;
import com.utilstore.customclasses.CustomCompleteToolBarMenuActivity;


public class StorePage extends CustomCompleteToolBarMenuActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_page);
        ILog logger = Logger.getLogger();
        try {
            StoreActions.setStoreActionInstance(this);
            super.assignDrawerLayout(StoreActions.getStoreActionInstance().getStoreDrawer());
            StoreActions.getStoreActionInstance().fetchStoreDepartments();
        } catch (Exception ex) {
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        StoreActions.getStoreActionInstance().destroyStoreActionInstance();
    }

    @Override
    public void onBackPressed() {
        if (StoreActions.getStoreActionInstance().getStoreDrawer().isDrawerOpen(Gravity.RIGHT)) {
            StoreActions.getStoreActionInstance().getStoreDrawer().closeDrawer(Gravity.RIGHT);
        } else {
            super.onBackPressed();
        }
    }
}
