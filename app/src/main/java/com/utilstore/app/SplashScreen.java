package com.utilstore.app;

import android.os.Bundle;

import com.utilstore.actions.SplashScreenActions;
import com.utilstore.customclasses.FontCompatActivity;

/**
 * Created by anfal on 12/9/2015.
 */
public class SplashScreen extends FontCompatActivity {

    private SplashScreenActions splashScreenActions = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen_page);
        this.splashScreenActions = new SplashScreenActions(this);
        this.splashScreenActions.initiateUserInfoCheck();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.splashScreenActions = null;
    }
}
