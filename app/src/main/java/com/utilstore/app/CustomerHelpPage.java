package com.utilstore.app;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;

import com.utilstore.actions.CustomerHelpActions;
import com.utilstore.analytics.logging.Constants;
import com.utilstore.analytics.logging.ILog;
import com.utilstore.analytics.logging.Logger;
import com.utilstore.customclasses.CustomCompleteToolBarMenuActivity;
import com.utilstore.utilities.Utilities;

/**
 * Created by anfal on 2/2/2016.
 */
public class CustomerHelpPage extends CustomCompleteToolBarMenuActivity {

    private ILog logger = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_help_page);

        this.logger = Logger.getLogger();

        try {
            CustomerHelpActions.setCustomerHelpActions(this);
            super.assignDrawerLayout(CustomerHelpActions.getCustomerHelpActions().getDrawerLayout());
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        CustomerHelpActions.getCustomerHelpActions().destroyCustomerHelpActions();
    }

    @Override
    public void onBackPressed() {
        try {
            if (CustomerHelpActions.getCustomerHelpActions().getDrawerLayout().isDrawerOpen(Gravity.RIGHT)) {
                CustomerHelpActions.getCustomerHelpActions().getDrawerLayout().closeDrawer(Gravity.RIGHT);
            } else {
                Utilities.getInstance().startNewActivityByFinishingOlder(this, StorePage.class);
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }
}
