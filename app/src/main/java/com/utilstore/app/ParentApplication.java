package com.utilstore.app;

import android.app.Application;

import com.flurry.android.FlurryAgent;

import static com.utilstore.analytics.logging.Constants.VERBOSE;
import static com.utilstore.constants.Constants.FLURRY_API_KEY;

/**
 * Created by anfal on 12/10/2015.
 */

public class ParentApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        FlurryAgent.init(this, FLURRY_API_KEY);
        FlurryAgent.setLogEnabled(true);
        FlurryAgent.setLogEvents(true);
        FlurryAgent.setVersionName(BuildConfig.VERSION_NAME);
        FlurryAgent.setLogLevel(VERBOSE);
    }
}
