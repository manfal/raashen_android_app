package com.utilstore.customclasses;

import android.util.Log;

import com.utilstore.analytics.logging.Constants;
import com.utilstore.analytics.logging.ILog;
import com.utilstore.app.StorePage;
import com.utilstore.utilities.Utilities;

import static com.utilstore.constants.Constants.shouldUserBeRedirectedToCheckoutPage;

/**
 * Created by anfal on 2/22/2016.
 */
public class CustomAccountActivity extends FontCompatActivity {

    private ILog logger = null;

    public void setLogger(ILog logger) {
        this.logger = logger;
    }

    @Override
    public void onBackPressed() {
        try {
            //This line just makes sure if user got to this page from checkout button but did not used it
            //then he should be redirected to checkout page by mistake.
            shouldUserBeRedirectedToCheckoutPage = false;
            Utilities.getInstance().startNewActivityByFinishingOlder(this, StorePage.class);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }
}
